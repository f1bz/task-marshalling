# JAXB/Jackson marshaller/unmarshaller for xml and json formats using reading/writing from/to database via JDBC

### Done for now (07.08.2019)

- Creating new parent with attributes
- Adding/deletion new children
- Xml file should have 2-3 attributues; 5 parents, each parent node should contain 2-3 children
- JSON/XML marshalling/unmarshalling via Jackson/JAXB
- JSON/XML validation using validation schemes
- Exception handling and usage of custom exceptions
- Logging via Logback with custom config
- Reading database properties
- Creating db on start from script
- Saving domain object parents and children to separate tables
- Reading data from db and writing it to CSV file
- Sql triggers on insertion 
- Clearing db on exit
- Reverting files changes
- Using design patterns (Strategy, Builder(Lombok), Command)
- Guava/ApacheCommons usage
- Lombok integration
- Junit tests with mocks and stub
- JavaDocs
- No Maven