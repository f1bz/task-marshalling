package com.mykolaiv.basecamp.summer2019.complextask01;

import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.parking.Parking;
import com.mykolaiv.basecamp.summer2019.complextask01.repository.ParkingRepository;
import com.mykolaiv.basecamp.summer2019.complextask01.service.command.Commands;
import com.mykolaiv.basecamp.summer2019.complextask01.service.exceptions.MarshallingException;
import com.mykolaiv.basecamp.summer2019.complextask01.service.exceptions.SchemaValidationException;
import com.mykolaiv.basecamp.summer2019.complextask01.service.exceptions.UncheckedSQLException;
import com.mykolaiv.basecamp.summer2019.complextask01.service.exceptions.WrongQueryParameterException;
import com.mykolaiv.basecamp.summer2019.complextask01.service.mode.Mode;
import com.mykolaiv.basecamp.summer2019.complextask01.service.persistence.JdbcManager;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UncheckedIOException;

/**
 * Main application controller for handling execution flow
 */
@Getter
@Setter
public class ApplicationController {

    public static final Logger logger = LoggerFactory.getLogger("default");

    private JdbcManager jdbcManager;
    private ParkingRepository parkingRepository;

    private Parking parking;
    private Mode currentMode;

    void start() {
        tryStart();
        for (Mode mode : Mode.values()) {
            currentMode = mode;
            logger.info("Switched to {} mode", mode);
            try {
                Commands.CREATE_NEW_PARKING.getCommand().executeWith(this);
                Commands.PARK_NEW_CARS.getCommand().executeWith(this);
                Commands.VIEW_IN_MEMORY_PARKING_MARSHALED.getCommand().executeWith(this);

                Commands.MARSHALLING_TO_FILE.getCommand().executeWith(this);
                Commands.STASH_FILE_CONTENT.getCommand().executeWith(this);

                Commands.UNPARK_FIRST_CAR.getCommand().executeWith(this);
                Commands.UNPARK_FIRST_CAR.getCommand().executeWith(this);
                Commands.UNPARK_FIRST_CAR.getCommand().executeWith(this);
                Commands.VIEW_IN_MEMORY_PARKING_MARSHALED.getCommand().executeWith(this);

                Commands.MARSHALLING_TO_FILE.getCommand().executeWith(this);
                Commands.PRINT_FILE_CONTENT.getCommand().executeWith(this);

                Commands.REVERT_FILE_CHANGES.getCommand().executeWith(this);
                Commands.PRINT_FILE_CONTENT.getCommand().executeWith(this);

                Commands.UNMARSHALLING_FROM_FILE.getCommand().executeWith(this);
                Commands.VIEW_IN_MEMORY_PARKING_MARSHALED.getCommand().executeWith(this);

                Commands.PERSIST_PARKING.getCommand().executeWith(this);
                Commands.PARKING_FROM_DATABASE_TO_CSV.getCommand().executeWith(this);
                Commands.RECREATE_DATABASE.getCommand().executeWith(this);
            } catch (UncheckedIOException e) {
                logger.error("Cannot read/write file!", e);
            } catch (UncheckedSQLException e) {
                logger.error("Something went wrong with execution SQL!", e);
            } catch (WrongQueryParameterException e) {
                logger.error("Wrong sql query parameter!", e);
            } catch (MarshallingException e) {
                logger.error("Cannot marshall/unmarshal!", e);
            } catch (SchemaValidationException e) {
                logger.error("Validation failed!", e);
            } catch (IllegalStateException | NullPointerException e) {
                logger.error("Illegal state or argument!", e);
            }
        }
        Commands.CLOSE_APPLICATION.getCommand().executeWith(this);
    }

    private void tryStart() {
        try {
            Commands.START_APPLICATION.getCommand().executeWith(this);
        } catch (Exception e) {
            logger.error("Cannot start application!", e);
            logger.info("Shutting down..");
            System.exit(1);
        }
    }
}