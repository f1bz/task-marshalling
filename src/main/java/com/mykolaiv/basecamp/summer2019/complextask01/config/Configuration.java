package com.mykolaiv.basecamp.summer2019.complextask01.config;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

import static org.apache.commons.lang3.StringUtils.EMPTY;

/**
 * Contains all configuration constants and provides
 * an opportunity to read properties file
 */
public class Configuration {

    private static final String RESOURCES_PATH = "src/main/resources/com/mykolaiv/basecamp/summer2019/complextask01/";

    public static final String LOGBACK_CONFIG_PATH = RESOURCES_PATH + "logback.xml";

    public static final String XSD_VALIDATION_SCHEMA_LOCATION = RESOURCES_PATH + "validation/parking-validation-scheme.xsd";
    public static final String JSON_VALIDATION_SCHEMA_LOCATION = RESOURCES_PATH + "validation/parking-validation-scheme.json";

    public static final String JSON_FILE_PATH = RESOURCES_PATH + "results/parking.json";
    public static final String XML_FILE_PATH = RESOURCES_PATH + "results/parking.xml";

    public static final String DATABASE_SCHEMA_CREATE_SCRIPT_PATH = RESOURCES_PATH + "database/create-schema.sql";
    public static final String DATABASE_SCHEMA_DROP_SCRIPT_PATH = RESOURCES_PATH + "database/drop-schema.sql";

    public static final String CSV_OUTPUT_PATH = RESOURCES_PATH + "results/data.csv";

    private static final String PROPERTIES_PATH = RESOURCES_PATH + "application.properties";
    private static Properties properties;

    private Configuration() {
        throw new IllegalStateException("Cannot create an instance of Configuration class!");
    }

    /**
     * Loads properties from file
     */
    public static void initProperties() throws IOException {
        if (properties == null) {
            properties = new Properties();
            properties.load(getReader());
        }
    }

    private static BufferedReader getReader() throws IOException {
        return Files.newBufferedReader(Paths.get(PROPERTIES_PATH));
    }

    /**
     * @param key key to find property
     * @return property by key or empty value if not found
     */
    public static String getPropertyValue(String key) {
        String value = properties.getProperty(key);
        if (value == null) {
            value = EMPTY;
        }
        return value;
    }
}