package com.mykolaiv.basecamp.summer2019.complextask01.model.credentials;

import lombok.Getter;

@Getter
public class DatabaseCredentials {

    private final String hostUrl;
    private final String user;
    private final String password;
    private final String driver;

    public DatabaseCredentials(String hostUrl, String user, String password, String driver) {
        this.hostUrl = hostUrl;
        this.user = user;
        this.password = password;
        this.driver = driver;
    }
}