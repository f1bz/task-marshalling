package com.mykolaiv.basecamp.summer2019.complextask01.model.domain;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@RequiredArgsConstructor
@XmlRootElement(name = "owner")
public class CarOwner {

    @XmlAttribute(name = "first-name")
    private final String firstName;

    @XmlAttribute(name = "last-name")
    private final String lastName;

    //for unmarshalling final fields
    private CarOwner() {
        this(null, null);
    }
}