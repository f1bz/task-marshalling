package com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.CarOwner;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.parts.CarConfigurationPart;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.parts.Engine;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.parts.Transmission;
import lombok.Data;
import lombok.experimental.SuperBuilder;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;
import java.util.ArrayList;
import java.util.List;

@Data
@SuperBuilder
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.WRAPPER_OBJECT)
@JsonSubTypes({
        @JsonSubTypes.Type(value = DefaultCar.class, name = "defaultCar"),
        @JsonSubTypes.Type(value = SportCar.class, name = "sportCar")
})
public abstract class Car {

    @XmlAttribute(name = "brand")
    private final String brand;

    @XmlAttribute(name = "registration-number")
    private final String registrationNumber;

    @XmlAttribute(name = "model")
    private final String model;

    @XmlAttribute(name = "priceInUsd")
    private final int priceInUsd;

    @XmlAttribute(name = "color")
    private final String color;

    private CarOwner carOwner;

    private List<CarConfigurationPart> carConfigurationPartList = new ArrayList<>();

    //for unmarshalling final fields
    Car() {
        this(null, null, null, 0, null);
    }

    Car(String registrationNumber, String brand, String model, int priceInUsd, String color) {
        this.registrationNumber = registrationNumber;
        this.brand = brand;
        this.model = model;
        this.priceInUsd = priceInUsd;
        this.color = color;
    }

    public void addConfigurationPart(CarConfigurationPart carConfigurationPart) {
        carConfigurationPartList.add(carConfigurationPart);
    }

    @JsonProperty("parts")
    @JsonSubTypes({
            @JsonSubTypes.Type(name = "transmission", value = Engine.class),
            @JsonSubTypes.Type(name = "engine", value = Transmission.class)
    })
    @XmlElementWrapper(name = "parts")
    @XmlElements({
            @XmlElement(name = "engine", type = Engine.class),
            @XmlElement(name = "transmission", type = Transmission.class)
    })
    public List<CarConfigurationPart> getCarConfigurationPartList() {
        return carConfigurationPartList;
    }

    @XmlElement(name = "owner")
    public CarOwner getCarOwner() {
        return carOwner;
    }
}