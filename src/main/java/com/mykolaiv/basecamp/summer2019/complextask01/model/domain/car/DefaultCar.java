package com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car;

import lombok.Builder;
import lombok.Data;

@Data
public class DefaultCar extends Car {

    // WORKAROUND!!!
    // Lombok plugin(v.0.25) for Intellij Idea doesn't support @SuperBuilder annotation for now
    @Builder
    public DefaultCar(String registrationNumber, String brand, String model, int priceInUsd, String color) {
        super(registrationNumber, brand, model, priceInUsd, color);
    }

    //for unmarshalling final fields
    public DefaultCar() {
        super(null, null, null, 0, null);
    }

}