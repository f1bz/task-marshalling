package com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.xml.bind.annotation.XmlAttribute;

@Data
@RequiredArgsConstructor
public class SportCar extends Car {

    @XmlAttribute(name = "turbo-charging-value")
    private final int turboChargingValue;

    // WORKAROUND!!!
    // Lombok plugin(v.0.25) for Intellij Idea doesn't support @SuperBuilder annotation for now
    @Builder
    public SportCar(String registrationNumber, String brand, String model, int priceInUsd, String color, int turboChargingValue) {
        super(registrationNumber, brand, model, priceInUsd, color);
        this.turboChargingValue = turboChargingValue;
    }

    //for unmarshalling final fields
    private SportCar() {
        super(null, null, null, 0, null);
        this.turboChargingValue = 0;
    }

}