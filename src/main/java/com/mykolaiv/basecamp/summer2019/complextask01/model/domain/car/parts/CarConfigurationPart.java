package com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.parts;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.WRAPPER_OBJECT)
@JsonSubTypes({
        @JsonSubTypes.Type(value = Engine.class, name = "engine"),
        @JsonSubTypes.Type(value = Transmission.class, name = "transmission")
})
public interface CarConfigurationPart {

}