package com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.parts;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@Builder
@RequiredArgsConstructor
@JsonRootName("engine")
@XmlRootElement(name = "engine")
public class Engine implements CarConfigurationPart {

    @XmlAttribute(name = "capacity")
    private final double capacity;

    @XmlAttribute(name = "type")
    private final String type;

    //for unmarshalling final fields
    private Engine() {
        this(0, null);
    }

    public double getCapacity() {
        return capacity;
    }

    public String getType() {
        return type;
    }
}
