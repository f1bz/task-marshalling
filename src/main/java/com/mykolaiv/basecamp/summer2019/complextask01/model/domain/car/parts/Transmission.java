package com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.parts;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@Builder
@RequiredArgsConstructor
@XmlRootElement(name = "transmission")
public class Transmission implements CarConfigurationPart {

    @JsonProperty("type")
    @XmlAttribute(name = "type")
    private final TransmissionType transmissionType;

    //for unmarshalling final fields
    private Transmission() {
        this(null);
    }

}