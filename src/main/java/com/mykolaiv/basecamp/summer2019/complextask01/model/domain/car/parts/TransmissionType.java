package com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.parts;

public enum TransmissionType {
    AUTO,
    MANUAL,
}