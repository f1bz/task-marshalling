package com.mykolaiv.basecamp.summer2019.complextask01.model.domain.parking;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.Car;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.DefaultCar;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.SportCar;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@Getter
@RequiredArgsConstructor
@JsonPropertyOrder({"title", "address", "parkedCars"})
@XmlRootElement(name = "parking")
public class Parking implements ParkingOperations {

    @XmlElement(name = "title")
    private final String title;

    @XmlElement
    private final String address;

    @XmlElementWrapper(name = "cars")
    @XmlElements({
            @XmlElement(name = "default-car", type = DefaultCar.class),
            @XmlElement(name = "sport-car", type = SportCar.class)
    })
    @JsonSubTypes({
            @JsonSubTypes.Type(name = "defaultCar", value = DefaultCar.class),
            @JsonSubTypes.Type(name = "sportCar", value = SportCar.class)
    })
    private List<Car> parkedCars = new ArrayList<>();

    //for unmarshalling final fields
    private Parking() {
        this(null, null);
    }

    public void parkCar(Car car) {
        parkedCars.add(car);
    }

    @Override
    public void unParkCar(int index) {
        if (!parkedCars.isEmpty() && parkedCars.size() - 1 >= index && index >= 0) {
            parkedCars.remove(index);
        }else{
            throw new IllegalArgumentException("Illegal index for unparking!");
        }
    }
}