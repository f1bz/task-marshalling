package com.mykolaiv.basecamp.summer2019.complextask01.model.domain.parking;

import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.Car;

/**
 * Provides operation set for parking object
 */
public interface ParkingOperations {

    /**
     * Add car to parking
     * @param car car to be added
     */
    void parkCar(Car car);

    /**
     * Remove car from parking by index
     * @param index of car to ber removed
     */
    void unParkCar(int index);

}