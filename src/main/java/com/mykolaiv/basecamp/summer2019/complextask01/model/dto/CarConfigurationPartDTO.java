package com.mykolaiv.basecamp.summer2019.complextask01.model.dto;

import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.parts.CarConfigurationPart;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.parts.Engine;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.parts.Transmission;
import lombok.Data;

/**
 * DTO class for CarConfigurationPart
 */
@Data
public class CarConfigurationPartDTO {

    private String name;
    private String type;
    private String characteristic;

    /**
     * Simple map CarConfigurationPart to CarConfigurationPartDTO
     * @param carConfigurationPart part to be mapped
     * @return CarConfigurationPartDTO created dto
     */
    public static CarConfigurationPartDTO mapToDTO(CarConfigurationPart carConfigurationPart) {
        CarConfigurationPartDTO carConfigurationPartDTO = new CarConfigurationPartDTO();
        if (carConfigurationPart instanceof Engine) {
            Engine engine = (Engine) carConfigurationPart;
            carConfigurationPartDTO.setName(Engine.class.getSimpleName());
            carConfigurationPartDTO.setType(engine.getType());
            carConfigurationPartDTO.setCharacteristic(String.valueOf(engine.getCapacity()));
        } else if (carConfigurationPart instanceof Transmission) {
            Transmission transmission = (Transmission) carConfigurationPart;
            carConfigurationPartDTO.setName(Transmission.class.getSimpleName());
            carConfigurationPartDTO.setType(transmission.getTransmissionType().name());
        }
        return carConfigurationPartDTO;
    }
}