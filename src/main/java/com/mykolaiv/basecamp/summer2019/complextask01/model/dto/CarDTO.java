package com.mykolaiv.basecamp.summer2019.complextask01.model.dto;

import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.CarOwner;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.Car;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.DefaultCar;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.SportCar;
import com.google.common.base.Preconditions;
import lombok.Data;

/**
 * DTO class for Car
 */
@Data
public class CarDTO {

    private String registrationNumber;
    private String type;
    private String brand;
    private String model;
    private int price;
    private String color;
    private String characteristic;
    private String ownerFirstName;
    private String ownerLastName;

    /**
     * Simple map Car to CarDTO
     * @param car to be mapped
     * @return CarDTO created dto
     */
    public static CarDTO mapToDTO(Car car) {
        Preconditions.checkNotNull(car);
        String type = DefaultCar.class.getSimpleName();
        String registrationNumber = car.getRegistrationNumber();
        String brand = car.getBrand();
        String model = car.getModel();
        String color = car.getColor();
        String characteristic = null;
        CarOwner carOwner = car.getCarOwner();
        String ownerFirstName = carOwner.getFirstName();
        String ownerLastName = carOwner.getLastName();
        int price = car.getPriceInUsd();
        if (car instanceof SportCar) {
            SportCar sportCar = (SportCar) car;
            characteristic = String.valueOf(sportCar.getTurboChargingValue());
            type = SportCar.class.getSimpleName();
        }
        CarDTO carDTO = new CarDTO();
        carDTO.setBrand(brand);
        carDTO.setType(type);
        carDTO.setRegistrationNumber(registrationNumber);
        carDTO.setModel(model);
        carDTO.setPrice(price);
        carDTO.setColor(color);
        carDTO.setCharacteristic(characteristic);
        carDTO.setOwnerFirstName(ownerFirstName);
        carDTO.setOwnerLastName(ownerLastName);
        return carDTO;
    }
}