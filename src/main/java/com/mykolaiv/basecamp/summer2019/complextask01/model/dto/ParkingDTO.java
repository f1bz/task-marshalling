package com.mykolaiv.basecamp.summer2019.complextask01.model.dto;

import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.Car;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.parking.Parking;
import com.google.common.base.Preconditions;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

/**
 * DTO class for Parking
 */
@Data
public class ParkingDTO {

    private final String title;
    private final String address;
    private List<CarDTO> carDTOs;
    private List<CarConfigurationPartDTO> carConfigurationPartDTOs;

    public ParkingDTO(String title, String address, List<CarDTO> carDTOs, List<CarConfigurationPartDTO> carConfigurationPartDTOs) {
        this.title = title;
        this.address = address;
        this.carDTOs = carDTOs;
        this.carConfigurationPartDTOs = carConfigurationPartDTOs;
    }

    /**
     * Simple map Parking to ParkingDTO
     * @param parking to be mapped
     * @return ParkingDTO created dto
     */
    public static ParkingDTO mapToDTO(Parking parking) {
        Preconditions.checkNotNull(parking);
        String title = parking.getTitle();
        String address = parking.getAddress();
        List<Car> parkedCars = parking.getParkedCars();
        List<CarDTO> carDTOs = parkedCars.stream()
                .map(CarDTO::mapToDTO)
                .collect(Collectors.toList());
        List<CarConfigurationPartDTO> carsCarConfigurationPartDTOs = parkedCars.stream()
                .map(Car::getCarConfigurationPartList)
                .flatMap(List::stream)
                .map(CarConfigurationPartDTO::mapToDTO)
                .collect(Collectors.toList());
        return new ParkingDTO(title, address, carDTOs, carsCarConfigurationPartDTOs);
    }
}
