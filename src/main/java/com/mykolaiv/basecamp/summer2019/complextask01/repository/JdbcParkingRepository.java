package com.mykolaiv.basecamp.summer2019.complextask01.repository;

import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.parking.Parking;
import com.mykolaiv.basecamp.summer2019.complextask01.model.dto.CarConfigurationPartDTO;
import com.mykolaiv.basecamp.summer2019.complextask01.model.dto.CarDTO;
import com.mykolaiv.basecamp.summer2019.complextask01.model.dto.ParkingDTO;
import com.mykolaiv.basecamp.summer2019.complextask01.repository.queries.Query;
import com.mykolaiv.basecamp.summer2019.complextask01.service.persistence.JdbcManager;

import java.util.List;

/**
 * Implementation of ParkingRepository using JDBC
 */
public class JdbcParkingRepository implements ParkingRepository {

    private JdbcManager jdbcManager;

    public JdbcParkingRepository(JdbcManager jdbcManager) {
        this.jdbcManager = jdbcManager;
    }

    @Override
    public void save(Parking parking) {
        ParkingDTO parkingDTO = ParkingDTO.mapToDTO(parking);
        jdbcManager.executeQuery(Query.INSERT_PARKING_QUERY, parkingDTO);
        jdbcManager.executeQuery(Query.INSERT_CARS_QUERY, parkingDTO.getCarDTOs());
        jdbcManager.executeQuery(Query.INSERT_CAR_PARTS_QUERY, parkingDTO.getCarConfigurationPartDTOs());
    }

    @Override
    public List<ParkingDTO> getParkingDTOs() {
        return jdbcManager.executeQuery(Query.GET_CAR_PARKING_QUERY);
    }

    @Override
    public List<CarDTO> getCarDTOs() {
        return jdbcManager.executeQuery(Query.GET_CARS_QUERY);
    }

    @Override
    public List<CarConfigurationPartDTO> getCarConfigurationPartDTOs() {
        return jdbcManager.executeQuery(Query.GET_CAR_PARTS_QUERY);
    }
}