package com.mykolaiv.basecamp.summer2019.complextask01.repository;

import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.parking.Parking;
import com.mykolaiv.basecamp.summer2019.complextask01.model.dto.CarConfigurationPartDTO;
import com.mykolaiv.basecamp.summer2019.complextask01.model.dto.CarDTO;
import com.mykolaiv.basecamp.summer2019.complextask01.model.dto.ParkingDTO;

import java.util.List;

/**
 * Repository for Parking
 */
public interface ParkingRepository {

    /**
     * Save Parking operation
     *
     * @param parking to be saved
     */
    void save(Parking parking);

    /**
     * Get list of carDTO objects
     *
     * @return List<CarDTO> from repository
     */
    List<CarDTO> getCarDTOs();

    /**
     * Get list of carConfigurationPartDTO objects
     *
     * @return List<CarConfigurationPartDTO>
     */
    List<CarConfigurationPartDTO> getCarConfigurationPartDTOs();

    /**
     * Get list of parkingDTO objects
     *
     * @return List<ParkingDTO>
     */
    List<ParkingDTO> getParkingDTOs();
}