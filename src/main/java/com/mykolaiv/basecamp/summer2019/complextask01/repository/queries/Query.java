package com.mykolaiv.basecamp.summer2019.complextask01.repository.queries;

import com.mykolaiv.basecamp.summer2019.complextask01.repository.queries.car.GetCarDTOsQuery;
import com.mykolaiv.basecamp.summer2019.complextask01.repository.queries.car.InsertCarsQuery;
import com.mykolaiv.basecamp.summer2019.complextask01.repository.queries.parking.GetParkingDTOsQuery;
import com.mykolaiv.basecamp.summer2019.complextask01.repository.queries.parking.InsertParkingQuery;
import com.mykolaiv.basecamp.summer2019.complextask01.repository.queries.parts.GetCarPartDTOsQuery;
import com.mykolaiv.basecamp.summer2019.complextask01.repository.queries.parts.InsertCarPartsQuery;
import lombok.Getter;

/**
 * Storage of all executable queries
 */
@Getter
public enum Query {

    INSERT_CAR_PARTS_QUERY(new InsertCarPartsQuery()),
    INSERT_CARS_QUERY(new InsertCarsQuery()),
    INSERT_PARKING_QUERY(new InsertParkingQuery()),

    GET_CAR_PARTS_QUERY(new GetCarPartDTOsQuery()),
    GET_CARS_QUERY(new GetCarDTOsQuery()),
    GET_CAR_PARKING_QUERY(new GetParkingDTOsQuery());

    private QueryExecutable queryExecutable;

    Query(QueryExecutable queryExecutable) {
        this.queryExecutable = queryExecutable;
    }
}