package com.mykolaiv.basecamp.summer2019.complextask01.repository.queries;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Command-like interface for JDBC queries implementation
 */
public interface QueryExecutable {

    /**
     * Execute query with params using JDBC connection
     *
     * @param connection JDBC connection
     * @param params     params to be used for queries
     * @return <T>       execution result object
     **/
    <T> T execute(Connection connection, Object... params) throws SQLException;

}