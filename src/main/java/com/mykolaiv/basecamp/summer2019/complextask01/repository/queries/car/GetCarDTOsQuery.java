package com.mykolaiv.basecamp.summer2019.complextask01.repository.queries.car;

import com.mykolaiv.basecamp.summer2019.complextask01.model.dto.CarDTO;
import com.mykolaiv.basecamp.summer2019.complextask01.repository.queries.QueryExecutable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GetCarDTOsQuery implements QueryExecutable {

    private static final String SELECT_CAR_PARTS_QUERY = "SELECT * FROM cars";

    @Override
    @SuppressWarnings("unchecked")
    public <T> T execute(Connection connection, Object... params) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_CAR_PARTS_QUERY)) {
            List<CarDTO> carDTOs = new ArrayList<>();
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    CarDTO carDTO = new CarDTO();
                    carDTO.setRegistrationNumber(resultSet.getString(1));
                    carDTO.setType(resultSet.getString(2));
                    carDTO.setBrand(resultSet.getString(3));
                    carDTO.setModel(resultSet.getString(4));
                    carDTO.setPrice(resultSet.getInt(5));
                    carDTO.setColor(resultSet.getString(6));
                    carDTO.setCharacteristic(resultSet.getString(7));
                    carDTO.setOwnerFirstName(resultSet.getString(8));
                    carDTO.setOwnerLastName(resultSet.getString(9));
                    carDTOs.add(carDTO);
                }
            }
            return (T) carDTOs;
        }
    }
}