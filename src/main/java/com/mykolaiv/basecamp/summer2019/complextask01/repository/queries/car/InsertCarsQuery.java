package com.mykolaiv.basecamp.summer2019.complextask01.repository.queries.car;

import com.mykolaiv.basecamp.summer2019.complextask01.model.dto.CarDTO;
import com.mykolaiv.basecamp.summer2019.complextask01.repository.queries.QueryExecutable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class InsertCarsQuery implements QueryExecutable {

    private static final String INSERT_CAR_SQL_QUERY = "INSERT INTO cars(number,type, brand, model, price_usd, " +
            "color, characteristic ,car_owner_first_name, car_owner_last_name) VALUES (?,?,?,?,?,?,?,?,?)";

    @Override
    @SuppressWarnings("unchecked")
    public <T> T execute(Connection connection, Object... params) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT_CAR_SQL_QUERY)) {
            connection.setAutoCommit(false);
            List<CarDTO> carDTOs = (ArrayList<CarDTO>) params[0];
            for (CarDTO carDTO : carDTOs) {
                preparedStatement.setString(1, carDTO.getRegistrationNumber());
                preparedStatement.setString(2, carDTO.getType());
                preparedStatement.setString(3, carDTO.getBrand());
                preparedStatement.setString(4, carDTO.getModel());
                preparedStatement.setInt(5, carDTO.getPrice());
                preparedStatement.setString(6, carDTO.getColor());
                preparedStatement.setString(7, carDTO.getCharacteristic());
                preparedStatement.setString(8, carDTO.getOwnerFirstName());
                preparedStatement.setString(9, carDTO.getOwnerLastName());
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
            connection.commit();
            return null;
        }
    }
}
