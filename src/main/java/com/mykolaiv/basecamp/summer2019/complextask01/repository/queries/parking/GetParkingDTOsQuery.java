package com.mykolaiv.basecamp.summer2019.complextask01.repository.queries.parking;

import com.mykolaiv.basecamp.summer2019.complextask01.model.dto.ParkingDTO;
import com.mykolaiv.basecamp.summer2019.complextask01.repository.queries.QueryExecutable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GetParkingDTOsQuery implements QueryExecutable {

    private static final String SELECT_PARKINGS_QUERY = "SELECT * FROM parking";

    @Override
    @SuppressWarnings("unchecked")
    public <T> T execute(Connection connection, Object... params) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_PARKINGS_QUERY)) {
            List<ParkingDTO> parkingDTOs = new ArrayList<>();
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    ParkingDTO parkingDTO = new ParkingDTO(resultSet.getString(1),
                            resultSet.getString(2), null, null);
                    parkingDTOs.add(parkingDTO);
                }
            }
            return (T) parkingDTOs;
        }
    }
}