package com.mykolaiv.basecamp.summer2019.complextask01.repository.queries.parking;

import com.mykolaiv.basecamp.summer2019.complextask01.model.dto.ParkingDTO;
import com.mykolaiv.basecamp.summer2019.complextask01.repository.queries.QueryExecutable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class InsertParkingQuery implements QueryExecutable {

    private static final String INSERT_CAR_SQL_QUERY = "INSERT INTO parking(title,address) VALUES (?,?)";

    @Override
    public <T> T execute(Connection connection, Object... params) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT_CAR_SQL_QUERY)) {
            ParkingDTO parkingDTO = (ParkingDTO) params[0];
            preparedStatement.setString(1, parkingDTO.getTitle());
            preparedStatement.setString(2, parkingDTO.getAddress());
            preparedStatement.addBatch();
            preparedStatement.executeBatch();
            return null;
        }
    }
}