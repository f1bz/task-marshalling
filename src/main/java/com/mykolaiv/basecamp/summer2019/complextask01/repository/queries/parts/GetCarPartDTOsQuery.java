package com.mykolaiv.basecamp.summer2019.complextask01.repository.queries.parts;

import com.mykolaiv.basecamp.summer2019.complextask01.model.dto.CarConfigurationPartDTO;
import com.mykolaiv.basecamp.summer2019.complextask01.repository.queries.QueryExecutable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GetCarPartDTOsQuery implements QueryExecutable {

	private static final String SELECT_CAR_PARTS_QUERY = "SELECT * FROM car_parts";

	@Override
	@SuppressWarnings("unchecked")
	public <T> T execute(Connection connection, Object... params) throws SQLException {
		try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_CAR_PARTS_QUERY)) {
			List<CarConfigurationPartDTO> carPartDTOs = new ArrayList<>();
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				while (resultSet.next()) {
					CarConfigurationPartDTO carConfigurationPartDTO = new CarConfigurationPartDTO();
					carConfigurationPartDTO.setName(resultSet.getString(1));
					carConfigurationPartDTO.setType(resultSet.getString(2));
					carConfigurationPartDTO.setCharacteristic(resultSet.getString(3));
					carPartDTOs.add(carConfigurationPartDTO);
				}
				return (T) carPartDTOs;
			}
		}
	}
}