package com.mykolaiv.basecamp.summer2019.complextask01.repository.queries.parts;

import com.mykolaiv.basecamp.summer2019.complextask01.model.dto.CarConfigurationPartDTO;
import com.mykolaiv.basecamp.summer2019.complextask01.repository.queries.QueryExecutable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class InsertCarPartsQuery implements QueryExecutable {

    private static final String INSERT_CAR_PARTS_SQL_QUERY = "INSERT INTO car_parts(name, type,other_characteristic) " +
            "VALUES (?,?,?)";

    @Override
    @SuppressWarnings("unchecked")
    public <T> T execute(Connection connection, Object... params) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT_CAR_PARTS_SQL_QUERY)) {
            connection.setAutoCommit(false);
            List<CarConfigurationPartDTO> carConfigurationPartDTOs = (ArrayList<CarConfigurationPartDTO>) params[0];
            for (CarConfigurationPartDTO carPartDTO : carConfigurationPartDTOs) {
                preparedStatement.setString(1, carPartDTO.getName());
                preparedStatement.setString(2, carPartDTO.getType());
                preparedStatement.setString(3, carPartDTO.getCharacteristic());
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
            connection.commit();
            return null;
        }
    }
}
