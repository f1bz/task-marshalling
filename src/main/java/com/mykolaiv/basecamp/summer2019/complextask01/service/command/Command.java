package com.mykolaiv.basecamp.summer2019.complextask01.service.command;

import com.mykolaiv.basecamp.summer2019.complextask01.ApplicationController;

/**
 * Command interface that use encapsulates logic for different aspects
 */
public interface Command {

    /**
     * Executes command operations
     *
     * @param applicationController controller for command to be applied for
     */
    void executeWith(ApplicationController applicationController);
}