package com.mykolaiv.basecamp.summer2019.complextask01.service.command;

import com.mykolaiv.basecamp.summer2019.complextask01.service.command.application.CloseCommand;
import com.mykolaiv.basecamp.summer2019.complextask01.service.command.application.StartCommand;
import com.mykolaiv.basecamp.summer2019.complextask01.service.command.domain.CreateNewParkingCommand;
import com.mykolaiv.basecamp.summer2019.complextask01.service.command.domain.ParkNewCarsCommand;
import com.mykolaiv.basecamp.summer2019.complextask01.service.command.domain.UnparkFirstCarCommand;
import com.mykolaiv.basecamp.summer2019.complextask01.service.command.file.InsertDataToCsvCommand;
import com.mykolaiv.basecamp.summer2019.complextask01.service.command.file.PrintResultFileCommand;
import com.mykolaiv.basecamp.summer2019.complextask01.service.command.file.RevertFileChangesCommand;
import com.mykolaiv.basecamp.summer2019.complextask01.service.command.file.StashFileContentCommand;
import com.mykolaiv.basecamp.summer2019.complextask01.service.command.marshalling.MarshalFileCommand;
import com.mykolaiv.basecamp.summer2019.complextask01.service.command.marshalling.UnmarshalFileCommand;
import com.mykolaiv.basecamp.summer2019.complextask01.service.command.marshalling.ValidateFileCommand;
import com.mykolaiv.basecamp.summer2019.complextask01.service.command.marshalling.ViewInMemoryMarshaledParkingCommand;
import com.mykolaiv.basecamp.summer2019.complextask01.service.command.persistence.CreateDatabaseCommand;
import com.mykolaiv.basecamp.summer2019.complextask01.service.command.persistence.DropDatabaseCommand;
import com.mykolaiv.basecamp.summer2019.complextask01.service.command.persistence.PersistParkingCommand;
import com.mykolaiv.basecamp.summer2019.complextask01.service.command.persistence.RecreateDatabaseCommand;
import lombok.Getter;

/**
 * Storage of all available commands for different aspects
 */
@Getter
public enum Commands {

    //Application commands
    START_APPLICATION(new StartCommand()),
    CLOSE_APPLICATION(new CloseCommand()),

    //Business logic commands
    CREATE_NEW_PARKING(new CreateNewParkingCommand()),
    PARK_NEW_CARS(new ParkNewCarsCommand()),
    UNPARK_FIRST_CAR(new UnparkFirstCarCommand()),

    //Marshalling/Unmarshalling/Validation commands
    MARSHALLING_TO_FILE(new MarshalFileCommand()),
    UNMARSHALLING_FROM_FILE(new UnmarshalFileCommand()),
    VIEW_IN_MEMORY_PARKING_MARSHALED(new ViewInMemoryMarshaledParkingCommand()),
    VALIDATE_FILE(new ValidateFileCommand()),

    //Persistence commands
    PERSIST_PARKING(new PersistParkingCommand()),
    CREATE_DATABASE(new CreateDatabaseCommand()),
    DROP_DATABASE(new DropDatabaseCommand()),
    RECREATE_DATABASE(new RecreateDatabaseCommand()),
    PARKING_FROM_DATABASE_TO_CSV(new InsertDataToCsvCommand()),

    //File commands
    PRINT_FILE_CONTENT(new PrintResultFileCommand()),
    STASH_FILE_CONTENT(new StashFileContentCommand()),
    REVERT_FILE_CHANGES(new RevertFileChangesCommand());

    private Command command;

    Commands(Command command) {
        this.command = command;
    }
}