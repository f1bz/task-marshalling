package com.mykolaiv.basecamp.summer2019.complextask01.service.command.application;

import com.mykolaiv.basecamp.summer2019.complextask01.ApplicationController;
import com.mykolaiv.basecamp.summer2019.complextask01.config.Configuration;
import com.mykolaiv.basecamp.summer2019.complextask01.service.command.Command;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static com.mykolaiv.basecamp.summer2019.complextask01.ApplicationController.logger;
import static com.mykolaiv.basecamp.summer2019.complextask01.service.command.Commands.DROP_DATABASE;

public class CloseCommand implements Command {

    @Override
    public void executeWith(ApplicationController applicationController) {
        try {
            logger.info("Clearing database up..");
            DROP_DATABASE.getCommand().executeWith(applicationController);
            String driver = Configuration.getPropertyValue("database.driver");
            if (driver.contains("sqlite")) {
                String url = Configuration.getPropertyValue("database.url");
                Files.delete(Paths.get(StringUtils.substringAfterLast(url, ":")));
            }
            logger.info("Database cleared up!");
            logger.info("Exiting..");
        } catch (IOException e) {
            logger.error("Oops, something went wrong!", e);
        }
    }
}
