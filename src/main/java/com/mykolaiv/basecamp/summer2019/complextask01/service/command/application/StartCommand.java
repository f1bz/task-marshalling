package com.mykolaiv.basecamp.summer2019.complextask01.service.command.application;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;
import com.mykolaiv.basecamp.summer2019.complextask01.ApplicationController;
import com.mykolaiv.basecamp.summer2019.complextask01.config.Configuration;
import com.mykolaiv.basecamp.summer2019.complextask01.model.credentials.DatabaseCredentials;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.parking.Parking;
import com.mykolaiv.basecamp.summer2019.complextask01.repository.JdbcParkingRepository;
import com.mykolaiv.basecamp.summer2019.complextask01.service.command.Command;
import com.mykolaiv.basecamp.summer2019.complextask01.service.exceptions.PropertiesNotFoundException;
import com.mykolaiv.basecamp.summer2019.complextask01.service.exceptions.UnknownJdbcDriverException;
import com.mykolaiv.basecamp.summer2019.complextask01.service.mode.Mode;
import com.mykolaiv.basecamp.summer2019.complextask01.service.mode.strategy.ModeStrategy;
import com.mykolaiv.basecamp.summer2019.complextask01.service.persistence.ConnectionManager;
import com.mykolaiv.basecamp.summer2019.complextask01.service.persistence.JdbcManager;
import com.mykolaiv.basecamp.summer2019.complextask01.utils.FilesUtils;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import static com.mykolaiv.basecamp.summer2019.complextask01.ApplicationController.logger;
import static com.mykolaiv.basecamp.summer2019.complextask01.config.Configuration.LOGBACK_CONFIG_PATH;
import static com.mykolaiv.basecamp.summer2019.complextask01.service.command.Commands.CREATE_DATABASE;
import static com.mykolaiv.basecamp.summer2019.complextask01.service.command.Commands.DROP_DATABASE;
import static org.apache.commons.lang3.StringUtils.EMPTY;

public class StartCommand implements Command {

    static {
        try {
            LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
            loggerContext.reset();
            JoranConfigurator configurator = new JoranConfigurator();
            InputStream configStream = org.apache.commons.io.FileUtils.openInputStream(new File(LOGBACK_CONFIG_PATH));
            configurator.setContext(loggerContext);
            configurator.doConfigure(configStream);
            configStream.close();
        } catch (JoranException | IOException e) {
            logger.error("Cannot find logback configuration.Using default logger config!");
        }
    }

    @Override
    public void executeWith(ApplicationController applicationController) {
        try {
            logger.info("Loading properties file..");
            Configuration.initProperties();
        } catch (IOException e) {
            throw new PropertiesNotFoundException("Cannot load properties file!", e);
        }
        loadFileContentForModes();
        applicationController.setParking(new Parking(null, null));
        setupDatabase(applicationController);
    }

    private void setupDatabase(ApplicationController applicationController) {
        ConnectionManager connectionManager = new ConnectionManager(readCredentials());
        JdbcManager jdbcManager = new JdbcManager(connectionManager);
        applicationController.setJdbcManager(jdbcManager);
        JdbcParkingRepository jdbcParkingRepository = new JdbcParkingRepository(jdbcManager);
        applicationController.setParkingRepository(jdbcParkingRepository);
        try {
            logger.info("Connecting to the database..");
            jdbcManager.testConnection();
            DROP_DATABASE.getCommand().executeWith(applicationController);
            logger.info("Creating SQL schema..");
            CREATE_DATABASE.getCommand().executeWith(applicationController);
            logger.info("Application has started!");
        } catch (ClassNotFoundException e) {
            throw new UnknownJdbcDriverException("Cannot load jdbc driver", e);
        }
    }

    private void loadFileContentForModes() {
        for (Mode mode : Mode.values()) {
            ModeStrategy modeStrategy = mode.getModeStrategy();
            try {
                modeStrategy.loadStashedFileContentFromFile();
            } catch (IOException e) {
                FilesUtils.toFile(modeStrategy.getFilePath(), EMPTY);
                logger.info("Result {} file not found. Empty value set!", mode);
            }
        }
    }

    private DatabaseCredentials readCredentials() {
        String url = Configuration.getPropertyValue("database.url");
        String password = Configuration.getPropertyValue("database.password");
        String user = Configuration.getPropertyValue("database.user");
        String driver = Configuration.getPropertyValue("database.driver");
        return new DatabaseCredentials(url, password, user, driver);
    }
}