package com.mykolaiv.basecamp.summer2019.complextask01.service.command.domain;

import com.mykolaiv.basecamp.summer2019.complextask01.ApplicationController;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.parking.Parking;
import com.mykolaiv.basecamp.summer2019.complextask01.service.command.Command;

import static com.mykolaiv.basecamp.summer2019.complextask01.ApplicationController.logger;

public class CreateNewParkingCommand implements Command {

    @Override
    public void executeWith(ApplicationController applicationController) {
        String title = "title";
        String address = "address";
        applicationController.setParking(new Parking(title, address));
        logger.info("New parking created!");
    }
}