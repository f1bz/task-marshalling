package com.mykolaiv.basecamp.summer2019.complextask01.service.command.domain;

import com.mykolaiv.basecamp.summer2019.complextask01.ApplicationController;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.CarOwner;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.Car;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.DefaultCar;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.SportCar;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.parts.CarConfigurationPart;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.parts.Engine;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.parts.Transmission;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.parts.TransmissionType;
import com.mykolaiv.basecamp.summer2019.complextask01.service.command.Command;

import java.util.Arrays;
import java.util.List;

import static com.mykolaiv.basecamp.summer2019.complextask01.ApplicationController.logger;

public class ParkNewCarsCommand implements Command {

    @Override
    public void executeWith(ApplicationController applicationController) {
        List<Car> cars = createNewCars();
        CarOwner carOwner = new CarOwner("Andrew", "Ivanov");
        cars.forEach(car -> car.setCarOwner(carOwner));
        cars.forEach(car -> applicationController.getParking().parkCar(car));
        logger.info("{} cars parked!", cars.size());
    }

    private List<Car> createNewCars() {
        Car car1 = createSportCar("AA 1234 AA");
        Car car2 = createDefaultCar("BB 1234 AA");
        Car car3 = createSportCar("CC 1234 AA");
        Car car4 = createDefaultCar("DD 1234 AA");
        Car car5 = createSportCar("EE 1234 AA");
        return Arrays.asList(car1, car2, car3, car4, car5);
    }

    private Car createSportCar(String regNumber) {
        Car sportCar = SportCar.builder()
                .brand("Nissan")
                .model("Gtr")
                .color("RED")
                .priceInUsd(100_000)
                .registrationNumber(regNumber)
                .turboChargingValue(6)
                .build();
        sportCar.setCarConfigurationPartList(createDefaultConfigurationParts());
        return sportCar;
    }

    private List<CarConfigurationPart> createSportConfigurationParts() {
        Engine engine = Engine.builder()
                .capacity(4)
                .type("Sport")
                .build();
        Transmission transmission = Transmission.builder()
                .transmissionType(TransmissionType.AUTO)
                .build();
        return Arrays.asList(engine, transmission);
    }

    private Car createDefaultCar(String regNumber) {
        Car sportCar = DefaultCar.builder()
                .brand("Opel")
                .model("Astra")
                .color("BLACK")
                .priceInUsd(10_000)
                .registrationNumber(regNumber)
                .build();
        sportCar.setCarConfigurationPartList(createSportConfigurationParts());
        return sportCar;
    }

    private List<CarConfigurationPart> createDefaultConfigurationParts() {
        Engine engine = Engine.builder()
                .capacity(2)
                .type("Dedaukt")
                .build();
        Transmission transmission = Transmission.builder()
                .transmissionType(TransmissionType.MANUAL)
                .build();
        return Arrays.asList(engine, transmission);
    }
}
