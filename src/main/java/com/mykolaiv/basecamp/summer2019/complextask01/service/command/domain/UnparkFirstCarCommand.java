package com.mykolaiv.basecamp.summer2019.complextask01.service.command.domain;

import com.mykolaiv.basecamp.summer2019.complextask01.ApplicationController;
import com.mykolaiv.basecamp.summer2019.complextask01.service.command.Command;

import static com.mykolaiv.basecamp.summer2019.complextask01.ApplicationController.logger;

public class UnparkFirstCarCommand implements Command {

    @Override
    public void executeWith(ApplicationController applicationController) {
        try {
            applicationController.getParking().unParkCar(0);
            logger.info("First car was unparked!");
        } catch (IllegalArgumentException e) {
            logger.info("First car wasn't unparked!", e);
        }
    }
}