package com.mykolaiv.basecamp.summer2019.complextask01.service.command.file;

import com.mykolaiv.basecamp.summer2019.complextask01.ApplicationController;
import com.mykolaiv.basecamp.summer2019.complextask01.model.dto.CarConfigurationPartDTO;
import com.mykolaiv.basecamp.summer2019.complextask01.model.dto.CarDTO;
import com.mykolaiv.basecamp.summer2019.complextask01.model.dto.ParkingDTO;
import com.mykolaiv.basecamp.summer2019.complextask01.repository.ParkingRepository;
import com.mykolaiv.basecamp.summer2019.complextask01.service.command.Command;
import com.mykolaiv.basecamp.summer2019.complextask01.utils.FilesUtils;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.List;

import static com.mykolaiv.basecamp.summer2019.complextask01.ApplicationController.logger;
import static com.mykolaiv.basecamp.summer2019.complextask01.config.Configuration.CSV_OUTPUT_PATH;

public class InsertDataToCsvCommand implements Command {

    private static final String PARKING_LABEL = "#### PARKINGS ####";
    private static final String[] PARKING_HEADERS = {
            "TITLE",
            "ADDRESS"
    };

    private static final String CARS_LABEL = "#### CARS ####";
    private static final String[] CARS_HEADERS = {
            "TYPE",
            "REGISTRATION NUMBER",
            "BRAND",
            "MODEL",
            "PRICE (USD)",
            "CHARACTERISTIC",
            "OWNER's FIRST NAME",
            "OWNER's LAST NAME"
    };

    private static final String CAR_PARTS_LABEL = "#### CAR PARTS ####";
    private static final String[] CAR_PARTS_HEADERS = {
            "NAME",
            "TYPE",
            "CHARACTERISTIC"
    };


    @Override
    public void executeWith(ApplicationController applicationController) {
        try {
            writeToFile(generateCsvData(applicationController));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private StringBuilder generateCsvData(ApplicationController applicationController) throws IOException {
        ParkingRepository parkingRepository = applicationController.getParkingRepository();
        List<CarConfigurationPartDTO> carPartDTOs = parkingRepository.getCarConfigurationPartDTOs();
        List<ParkingDTO> parkingDTOs = parkingRepository.getParkingDTOs();
        List<CarDTO> carDTOs = parkingRepository.getCarDTOs();

        StringBuilder appendingResultCollector = new StringBuilder();
        try (CSVPrinter printer = new CSVPrinter(appendingResultCollector, CSVFormat.DEFAULT)) {
            appendParkings(parkingDTOs, printer);
            printIndents(printer);
            appendCars(carDTOs, printer);
            printIndents(printer);
            appendParts(carPartDTOs, printer);
            logger.info("Data was successfully written to CSV!");
        }
        return appendingResultCollector;
    }

    private void writeToFile(StringBuilder appendingResultCollector) {
        FilesUtils.toFile(CSV_OUTPUT_PATH, appendingResultCollector.toString());
    }

    private void appendParkings(List<ParkingDTO> parkingDTOs, CSVPrinter printer) throws IOException {
        printer.printRecord(PARKING_LABEL);
        printer.printRecord((Object[]) PARKING_HEADERS);
        for (ParkingDTO p : parkingDTOs) {
            printer.printRecord(
                    p.getTitle(),
                    p.getAddress()
            );
        }
    }

    private void appendCars(List<CarDTO> carDTOs, CSVPrinter printer) throws IOException {
        printer.printRecord(CARS_LABEL);
        printer.printRecord((Object[]) CARS_HEADERS);
        for (CarDTO c : carDTOs) {
            printer.printRecord(
                    c.getRegistrationNumber(),
                    c.getBrand(),
                    c.getModel(),
                    c.getPrice(),
                    c.getColor(),
                    c.getCharacteristic(),
                    c.getOwnerFirstName(),
                    c.getOwnerLastName()
            );
        }
    }

    private void appendParts(List<CarConfigurationPartDTO> carPartDTOs, CSVPrinter printer) throws IOException {
        printer.printRecord(CAR_PARTS_LABEL);
        printer.printRecord((Object[]) CAR_PARTS_HEADERS);
        for (CarConfigurationPartDTO c : carPartDTOs) {
            printer.printRecord(
                    c.getName(),
                    c.getType(),
                    c.getCharacteristic()
            );
        }
    }

    private void printIndents(CSVPrinter printer) throws IOException {
        printer.println();
        printer.println();
        printer.println();
    }
}