package com.mykolaiv.basecamp.summer2019.complextask01.service.command.file;

import com.mykolaiv.basecamp.summer2019.complextask01.ApplicationController;
import com.mykolaiv.basecamp.summer2019.complextask01.service.command.Command;
import com.mykolaiv.basecamp.summer2019.complextask01.service.mode.strategy.ModeStrategy;

import java.io.IOException;
import java.io.UncheckedIOException;

import static com.mykolaiv.basecamp.summer2019.complextask01.ApplicationController.*;

public class PrintResultFileCommand implements Command {

    @Override
    public void executeWith(ApplicationController applicationController) {
        ModeStrategy modeStrategy = applicationController.getCurrentMode().getModeStrategy();
        try {
            String fileContent = getFileContent(modeStrategy);
            logger.info("File content\n{}", fileContent);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private String getFileContent(ModeStrategy modeStrategy) throws IOException {
        return modeStrategy.readFileContent();
    }
}