package com.mykolaiv.basecamp.summer2019.complextask01.service.command.file;

import com.mykolaiv.basecamp.summer2019.complextask01.ApplicationController;
import com.mykolaiv.basecamp.summer2019.complextask01.service.command.Command;
import com.mykolaiv.basecamp.summer2019.complextask01.service.mode.Mode;
import com.mykolaiv.basecamp.summer2019.complextask01.service.mode.strategy.ModeStrategy;

import java.io.IOException;
import java.io.UncheckedIOException;

import static com.mykolaiv.basecamp.summer2019.complextask01.ApplicationController.logger;

public class StashFileContentCommand implements Command {

    @Override
    public void executeWith(ApplicationController applicationController) {
        Mode mode = applicationController.getCurrentMode();
        ModeStrategy modeStrategy = mode.getModeStrategy();
        try {
            modeStrategy.loadStashedFileContentFromFile();
            logger.info("File content stashed!");
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}