package com.mykolaiv.basecamp.summer2019.complextask01.service.command.marshalling;

import com.mykolaiv.basecamp.summer2019.complextask01.ApplicationController;
import com.mykolaiv.basecamp.summer2019.complextask01.service.command.Command;
import com.mykolaiv.basecamp.summer2019.complextask01.service.exceptions.MarshallingException;
import com.mykolaiv.basecamp.summer2019.complextask01.service.mode.strategy.ModeStrategy;

import java.io.IOException;

import static com.mykolaiv.basecamp.summer2019.complextask01.ApplicationController.logger;

public class MarshalFileCommand implements Command {

    @Override
    public void executeWith(ApplicationController applicationController) {
        try {
            ModeStrategy modeStrategy = applicationController.getCurrentMode().getModeStrategy();
            modeStrategy.marshalParking(applicationController.getParking());
            logger.info("Marshaled to file!");
        } catch (IOException e) {
            throw new MarshallingException("Cannot marshal object", e);
        }
    }
}
