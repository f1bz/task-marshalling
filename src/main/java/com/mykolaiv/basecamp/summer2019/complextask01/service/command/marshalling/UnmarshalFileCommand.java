package com.mykolaiv.basecamp.summer2019.complextask01.service.command.marshalling;

import com.mykolaiv.basecamp.summer2019.complextask01.ApplicationController;
import com.mykolaiv.basecamp.summer2019.complextask01.service.exceptions.MarshallingException;
import com.mykolaiv.basecamp.summer2019.complextask01.service.command.Command;
import com.mykolaiv.basecamp.summer2019.complextask01.service.mode.strategy.ModeStrategy;

import java.io.IOException;
import java.io.UncheckedIOException;

import static com.mykolaiv.basecamp.summer2019.complextask01.ApplicationController.logger;

public class UnmarshalFileCommand implements Command {

    @Override
    public void executeWith(ApplicationController applicationController) {
        try {
            ModeStrategy modeStrategy = applicationController.getCurrentMode().getModeStrategy();
            applicationController.setParking(modeStrategy.unmarshalParking());
            logger.info("Unmarshaled from file!");
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }catch (Exception e){
            throw new MarshallingException("Cannot unmarshal to object", e);
        }
    }
}
