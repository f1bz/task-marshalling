package com.mykolaiv.basecamp.summer2019.complextask01.service.command.marshalling;

import com.mykolaiv.basecamp.summer2019.complextask01.ApplicationController;
import com.mykolaiv.basecamp.summer2019.complextask01.service.exceptions.SchemaValidationException;
import com.mykolaiv.basecamp.summer2019.complextask01.service.command.Command;
import com.mykolaiv.basecamp.summer2019.complextask01.service.mode.Mode;
import com.mykolaiv.basecamp.summer2019.complextask01.service.mode.strategy.ModeStrategy;
import org.everit.json.schema.SchemaException;
import org.everit.json.schema.ValidationException;
import org.json.JSONException;

import java.io.IOException;
import java.io.UncheckedIOException;

import static com.mykolaiv.basecamp.summer2019.complextask01.ApplicationController.logger;

public class ValidateFileCommand implements Command {

    @Override
    public void executeWith(ApplicationController applicationController) {
        Mode mode = applicationController.getCurrentMode();
        ModeStrategy modeStrategy = mode.getModeStrategy();
        try {
            logger.info("Validating {} file..", mode);
            modeStrategy.validateFile();
            logger.info("Validation succeeded!");
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        } catch (JSONException | SchemaException | ValidationException e) {
            throw new SchemaValidationException(e);
        }
    }
}
