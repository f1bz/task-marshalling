package com.mykolaiv.basecamp.summer2019.complextask01.service.command.marshalling;

import com.mykolaiv.basecamp.summer2019.complextask01.ApplicationController;
import com.mykolaiv.basecamp.summer2019.complextask01.service.exceptions.MarshallingException;
import com.mykolaiv.basecamp.summer2019.complextask01.service.command.Command;
import com.mykolaiv.basecamp.summer2019.complextask01.service.mode.strategy.ModeStrategy;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.UncheckedIOException;

import static com.mykolaiv.basecamp.summer2019.complextask01.ApplicationController.logger;

public class ViewInMemoryMarshaledParkingCommand implements Command {

    @Override
    public void executeWith(ApplicationController applicationController) {
        try {
            ModeStrategy modeStrategy = applicationController.getCurrentMode().getModeStrategy();
            String json = modeStrategy.getMarshalledParking(applicationController.getParking());
            logger.info("Marshaled parking from memory: \n{}", json);
        } catch (JAXBException e) {
            throw new MarshallingException("Cannot marshal/unmarshal object!", e);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
