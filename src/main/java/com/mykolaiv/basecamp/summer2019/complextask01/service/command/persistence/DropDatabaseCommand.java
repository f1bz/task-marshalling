package com.mykolaiv.basecamp.summer2019.complextask01.service.command.persistence;

import com.mykolaiv.basecamp.summer2019.complextask01.ApplicationController;
import com.mykolaiv.basecamp.summer2019.complextask01.service.command.Command;
import com.mykolaiv.basecamp.summer2019.complextask01.utils.FilesUtils;

import java.io.IOException;
import java.io.UncheckedIOException;

import static com.mykolaiv.basecamp.summer2019.complextask01.config.Configuration.DATABASE_SCHEMA_DROP_SCRIPT_PATH;

public class DropDatabaseCommand implements Command {

    @Override
    public void executeWith(ApplicationController applicationController) {
        try {
            String script = FilesUtils.fromFile(DATABASE_SCHEMA_DROP_SCRIPT_PATH);
            applicationController.getJdbcManager().executeScript(script);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}