package com.mykolaiv.basecamp.summer2019.complextask01.service.command.persistence;

import com.mykolaiv.basecamp.summer2019.complextask01.ApplicationController;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.parking.Parking;
import com.mykolaiv.basecamp.summer2019.complextask01.repository.ParkingRepository;
import com.mykolaiv.basecamp.summer2019.complextask01.service.command.Command;

import static com.mykolaiv.basecamp.summer2019.complextask01.ApplicationController.logger;

public class PersistParkingCommand implements Command {

    @Override
    public void executeWith(ApplicationController applicationController) {
        ParkingRepository parkingRepository = applicationController.getParkingRepository();
        Parking parking = applicationController.getParking();
        if (parking.getTitle() == null) {
            throw new IllegalStateException("Title of the parking cannot be null!");
        }
        parkingRepository.save(parking);
        logger.info("Parking saved!");
    }
}