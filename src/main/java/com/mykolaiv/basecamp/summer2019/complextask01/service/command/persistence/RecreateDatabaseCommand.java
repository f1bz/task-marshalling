package com.mykolaiv.basecamp.summer2019.complextask01.service.command.persistence;

import com.mykolaiv.basecamp.summer2019.complextask01.ApplicationController;
import com.mykolaiv.basecamp.summer2019.complextask01.service.command.Command;
import com.mykolaiv.basecamp.summer2019.complextask01.service.command.Commands;

import static com.mykolaiv.basecamp.summer2019.complextask01.ApplicationController.logger;

public class RecreateDatabaseCommand implements Command {

    @Override
    public void executeWith(ApplicationController applicationController) {
        Commands.DROP_DATABASE.getCommand().executeWith(applicationController);
        Commands.CREATE_DATABASE.getCommand().executeWith(applicationController);
        logger.info("Database recreated!");
    }
}