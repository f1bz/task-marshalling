package com.mykolaiv.basecamp.summer2019.complextask01.service.exceptions;

public class MarshallingException extends RuntimeException {

    public MarshallingException(String message, Exception e) {
        super(message, e);
    }

    public MarshallingException(Exception e) {
        super(e);
    }
}
