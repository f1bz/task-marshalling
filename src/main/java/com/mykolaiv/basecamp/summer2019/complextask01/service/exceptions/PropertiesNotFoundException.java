package com.mykolaiv.basecamp.summer2019.complextask01.service.exceptions;

import java.io.IOException;

public class PropertiesNotFoundException extends RuntimeException {

    public PropertiesNotFoundException(String message, IOException e) {
        super(message,e);
    }
}
