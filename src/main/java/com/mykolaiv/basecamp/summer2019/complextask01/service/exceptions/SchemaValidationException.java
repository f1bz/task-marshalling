package com.mykolaiv.basecamp.summer2019.complextask01.service.exceptions;

public class SchemaValidationException extends RuntimeException {

    public SchemaValidationException(Exception e) {
        super(e);
    }
}
