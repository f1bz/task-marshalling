package com.mykolaiv.basecamp.summer2019.complextask01.service.exceptions;

import java.sql.SQLException;

public class UncheckedSQLException extends RuntimeException {

    public UncheckedSQLException(SQLException cause) {
        super(cause);
    }

}
