package com.mykolaiv.basecamp.summer2019.complextask01.service.exceptions;

public class UnknownJdbcDriverException extends RuntimeException {

    public UnknownJdbcDriverException(String message, Exception e) {
        super(message, e);
    }
}
