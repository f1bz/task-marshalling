package com.mykolaiv.basecamp.summer2019.complextask01.service.exceptions;

public class WrongQueryParameterException extends RuntimeException {

    public WrongQueryParameterException(String message, Exception e) {
        super(message, e);
    }

}