package com.mykolaiv.basecamp.summer2019.complextask01.service.marshallers;

import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.parking.Parking;
import com.mykolaiv.basecamp.summer2019.complextask01.service.marshallers.json.ParkingJsonMarshaller;
import com.mykolaiv.basecamp.summer2019.complextask01.service.marshallers.json.ParkingJsonUnmarshaller;
import com.mykolaiv.basecamp.summer2019.complextask01.service.marshallers.json.SimpleParkingJsonMarshaller;
import com.mykolaiv.basecamp.summer2019.complextask01.service.marshallers.json.SimpleParkingJsonUnmarshaller;
import com.mykolaiv.basecamp.summer2019.complextask01.service.marshallers.json.ValidationParkingJsonUnmarshaller;
import com.mykolaiv.basecamp.summer2019.complextask01.service.marshallers.xml.ParkingXmlMarshaller;
import com.mykolaiv.basecamp.summer2019.complextask01.service.marshallers.xml.ParkingXmlUnmarshaller;
import com.mykolaiv.basecamp.summer2019.complextask01.service.marshallers.xml.SimpleParkingXmlMarshaller;
import com.mykolaiv.basecamp.summer2019.complextask01.service.marshallers.xml.SimpleParkingXmlUnmarshaller;
import com.mykolaiv.basecamp.summer2019.complextask01.service.marshallers.xml.ValidationParkingXmlUnmarshaller;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBException;
import java.io.IOException;

/**
 * Marshalling helper that uses default marshaller/unmarshaller
 */
public class MarshallingHelper {

    private static final ParkingXmlMarshaller PARKING_XML_MARSHALLER = new SimpleParkingXmlMarshaller();

    private static final ParkingXmlUnmarshaller PARKING_XML_UNMARSHALLER = new SimpleParkingXmlUnmarshaller();

    private static final ParkingJsonMarshaller PARKING_JSON_MARSHALLER = new SimpleParkingJsonMarshaller();

    private static final ParkingJsonUnmarshaller PARKING_JSON_UNMARSHALLER = new SimpleParkingJsonUnmarshaller();

    private static final ParkingXmlUnmarshaller PARKING_XML_VALIDATION_UNMARSHALLER = new ValidationParkingXmlUnmarshaller();

    private static final ParkingJsonUnmarshaller PARKING_JSON_VALIDATION_UNMARSHALLER = new ValidationParkingJsonUnmarshaller();

    private MarshallingHelper() {
        throw new IllegalStateException("Cannot create an instance of utility class!");
    }

    public static String toXml(Parking parking) throws JAXBException {
        return PARKING_XML_MARSHALLER.marshall(parking);
    }

    public static Parking fromXml(String xml) throws JAXBException, SAXException {
        return PARKING_XML_UNMARSHALLER.unmarshall(xml);
    }

    public static String toJson(Parking parking) throws JsonProcessingException {
        return PARKING_JSON_MARSHALLER.marshall(parking);
    }

    public static Parking fromJson(String json) throws IOException {
        return PARKING_JSON_UNMARSHALLER.unmarshall(json);
    }

    public static void validateJson(String json) throws IOException {
        PARKING_JSON_VALIDATION_UNMARSHALLER.unmarshall(json);
    }

    public static void validateXml(String xml) throws JAXBException, SAXException {
        PARKING_XML_VALIDATION_UNMARSHALLER.unmarshall(xml);
    }
}