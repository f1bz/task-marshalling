package com.mykolaiv.basecamp.summer2019.complextask01.service.marshallers.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.parking.Parking;

/**
 * JSON parking marshaller
 */
public interface ParkingJsonMarshaller {

    String marshall(Parking parking) throws JsonProcessingException;

}