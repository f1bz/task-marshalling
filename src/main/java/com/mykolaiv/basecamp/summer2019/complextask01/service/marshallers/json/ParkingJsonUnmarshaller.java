package com.mykolaiv.basecamp.summer2019.complextask01.service.marshallers.json;

import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.parking.Parking;

import java.io.IOException;

/**
 * JSON parking unmarshaller
 */
public interface ParkingJsonUnmarshaller {

    Parking unmarshall(String json) throws IOException;

}