package com.mykolaiv.basecamp.summer2019.complextask01.service.marshallers.json;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.parking.Parking;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.DefaultCar;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.SportCar;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.parts.Engine;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.parts.Transmission;

public class SimpleParkingJsonMarshaller implements ParkingJsonMarshaller {

    private final ObjectWriter objectWriter;

    public SimpleParkingJsonMarshaller() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibilityChecker(objectMapper.getSerializationConfig().getDefaultVisibilityChecker()
                .withFieldVisibility(JsonAutoDetect.Visibility.ANY));
        objectMapper.registerSubtypes(SportCar.class, DefaultCar.class, Transmission.class, Engine.class);
        this.objectWriter = objectMapper.writer().withDefaultPrettyPrinter();
    }

    @Override
    public String marshall(Parking parking) throws JsonProcessingException {
        return objectWriter.writeValueAsString(parking);
    }
}