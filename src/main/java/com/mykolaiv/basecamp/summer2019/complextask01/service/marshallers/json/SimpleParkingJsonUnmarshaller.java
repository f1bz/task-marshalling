package com.mykolaiv.basecamp.summer2019.complextask01.service.marshallers.json;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.DefaultCar;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.SportCar;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.parts.Engine;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.parts.Transmission;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.parking.Parking;

import java.io.IOException;

public class SimpleParkingJsonUnmarshaller implements ParkingJsonUnmarshaller {

    private final ObjectMapper objectMapper;

    public SimpleParkingJsonUnmarshaller() {
        objectMapper = new ObjectMapper();
        objectMapper.setVisibilityChecker(objectMapper.getSerializationConfig().getDefaultVisibilityChecker()
                .withFieldVisibility(JsonAutoDetect.Visibility.ANY));
        objectMapper.registerSubtypes(SportCar.class, DefaultCar.class, Transmission.class, Engine.class);
    }

    @Override
    public Parking unmarshall(String json) throws IOException {
        return objectMapper.readValue(json, Parking.class);
    }
}