package com.mykolaiv.basecamp.summer2019.complextask01.service.marshallers.json;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.DefaultCar;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.SportCar;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.parts.Engine;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.parts.Transmission;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.parking.Parking;
import com.mykolaiv.basecamp.summer2019.complextask01.utils.FilesUtils;
import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.IOException;
import java.io.StringReader;

import static com.mykolaiv.basecamp.summer2019.complextask01.config.Configuration.JSON_VALIDATION_SCHEMA_LOCATION;

public class ValidationParkingJsonUnmarshaller implements ParkingJsonUnmarshaller {

    private final ObjectMapper objectMapper;

    public ValidationParkingJsonUnmarshaller() {
        objectMapper = new ObjectMapper();
        objectMapper.setVisibilityChecker(objectMapper.getSerializationConfig().getDefaultVisibilityChecker()
                .withFieldVisibility(JsonAutoDetect.Visibility.ANY));
        objectMapper.registerSubtypes(SportCar.class, DefaultCar.class, Transmission.class, Engine.class);
    }

    @Override
    public Parking unmarshall(String json) throws IOException {
        StringReader jsonStringReader = new StringReader(json);
        StringReader schemaStringReader = new StringReader(FilesUtils.fromFile(JSON_VALIDATION_SCHEMA_LOCATION));
        JSONObject jsonSubject = new JSONObject(new JSONTokener(jsonStringReader));
        JSONObject jsonSchemaObject = new JSONObject(new JSONTokener(schemaStringReader));
        SchemaLoader schemaLoader = SchemaLoader.builder()
                .schemaJson(jsonSchemaObject)
                .build();
        Schema schema = schemaLoader.load().build();
        schema.validate(jsonSubject);
        return objectMapper.readValue(json, Parking.class);
    }
}