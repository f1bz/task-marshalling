package com.mykolaiv.basecamp.summer2019.complextask01.service.marshallers.xml;

import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.parking.Parking;

import javax.xml.bind.JAXBException;

/**
 * XML parking marshaller
 */
public interface ParkingXmlMarshaller {

    String marshall(Parking parking) throws JAXBException;

}
