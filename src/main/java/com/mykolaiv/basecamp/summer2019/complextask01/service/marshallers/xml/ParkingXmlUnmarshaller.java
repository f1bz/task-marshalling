package com.mykolaiv.basecamp.summer2019.complextask01.service.marshallers.xml;

import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.parking.Parking;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBException;
/**
 * XML parking unmarshaller
 */
public interface ParkingXmlUnmarshaller {

    Parking unmarshall(String xml) throws JAXBException, SAXException;

}