package com.mykolaiv.basecamp.summer2019.complextask01.service.marshallers.xml;

import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.parking.Parking;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;

public class SimpleParkingXmlMarshaller implements ParkingXmlMarshaller {

    @Override
    public String marshall(Parking parking) throws JAXBException {
        StringWriter marshallingResultStringWriter = new StringWriter();
        JAXBContext jaxbContext = JAXBContext.newInstance(Parking.class);
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.marshal(parking, marshallingResultStringWriter);
        return marshallingResultStringWriter.toString();
    }
}