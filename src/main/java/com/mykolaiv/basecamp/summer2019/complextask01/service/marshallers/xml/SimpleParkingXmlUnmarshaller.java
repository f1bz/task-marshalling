package com.mykolaiv.basecamp.summer2019.complextask01.service.marshallers.xml;

import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.parking.Parking;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;

public class SimpleParkingXmlUnmarshaller implements ParkingXmlUnmarshaller {

    @Override
    public Parking unmarshall(String xml) throws JAXBException {
        StringReader stringReader = new StringReader(xml);
        JAXBContext jaxbContext = JAXBContext.newInstance(Parking.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        return (Parking) unmarshaller.unmarshal(stringReader);
    }
}