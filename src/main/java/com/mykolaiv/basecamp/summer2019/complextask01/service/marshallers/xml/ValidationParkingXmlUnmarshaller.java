package com.mykolaiv.basecamp.summer2019.complextask01.service.marshallers.xml;

import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.parking.Parking;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.SchemaFactory;
import java.io.StringReader;

import static com.mykolaiv.basecamp.summer2019.complextask01.config.Configuration.XSD_VALIDATION_SCHEMA_LOCATION;

public class ValidationParkingXmlUnmarshaller implements ParkingXmlUnmarshaller {

    @Override
    public Parking unmarshall(String xml) throws JAXBException, SAXException {
        StringReader stringReader = new StringReader(xml);
        JAXBContext jaxbContext = JAXBContext.newInstance(Parking.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        javax.xml.validation.Schema schema = sf.newSchema(new java.io.File(XSD_VALIDATION_SCHEMA_LOCATION));
        unmarshaller.setSchema(schema);
        return (Parking) unmarshaller.unmarshal(stringReader);
    }
}