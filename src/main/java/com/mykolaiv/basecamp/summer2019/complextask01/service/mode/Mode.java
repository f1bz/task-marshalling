package com.mykolaiv.basecamp.summer2019.complextask01.service.mode;

import com.mykolaiv.basecamp.summer2019.complextask01.service.mode.strategy.JsonModeStrategy;
import com.mykolaiv.basecamp.summer2019.complextask01.service.mode.strategy.ModeStrategy;
import com.mykolaiv.basecamp.summer2019.complextask01.service.mode.strategy.XmlModeStrategy;
import lombok.Getter;

/**
 * ModeStrategy holder
 */
@Getter
public enum Mode {

    JSON(new JsonModeStrategy()),
    XML(new XmlModeStrategy());

    private ModeStrategy modeStrategy;

    Mode(ModeStrategy modeStrategy) {
        this.modeStrategy = modeStrategy;
    }
}