package com.mykolaiv.basecamp.summer2019.complextask01.service.mode.strategy;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.parking.Parking;
import com.mykolaiv.basecamp.summer2019.complextask01.service.exceptions.SchemaValidationException;
import com.mykolaiv.basecamp.summer2019.complextask01.service.marshallers.MarshallingHelper;
import com.mykolaiv.basecamp.summer2019.complextask01.utils.FilesUtils;

import java.io.IOException;

import static com.mykolaiv.basecamp.summer2019.complextask01.config.Configuration.JSON_FILE_PATH;

/**
 * Application working mode that provides work with JSON
 */
public class JsonModeStrategy extends ModeStrategy {

    public JsonModeStrategy() {
        super(JSON_FILE_PATH);
    }

    @Override
    public String getMarshalledParking(Parking parking) throws IOException {
        return MarshallingHelper.toJson(parking);
    }

    @Override
    public void marshalParking(Parking parking) throws IOException {
        String marshaledParkingJson = getMarshalledParking(parking);
        FilesUtils.toFile(getFilePath(), marshaledParkingJson, false);
    }

    @Override
    public Parking unmarshalParking() throws IOException {
        return MarshallingHelper.fromJson(readFileContent());
    }

    @Override
    public void validateFile() throws IOException {
        try {
            MarshallingHelper.validateJson(readFileContent());
        }catch (JsonProcessingException e){
            throw new SchemaValidationException(e);
        }
    }
}