package com.mykolaiv.basecamp.summer2019.complextask01.service.mode.strategy;

import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.parking.Parking;
import com.mykolaiv.basecamp.summer2019.complextask01.utils.FilesUtils;
import lombok.Getter;

import javax.xml.bind.JAXBException;
import java.io.IOException;

import static org.apache.commons.lang3.StringUtils.EMPTY;

/**
 * Application working mode
 */
@Getter
public abstract class ModeStrategy {

    private String filePath;
    private String stashedFileContent;

    public ModeStrategy(String filePath) {
        this.filePath = filePath;
    }

    public String readFileContent() throws IOException {
        return FilesUtils.fromFile(filePath);
    }

    public void loadStashedFileContentFromFile() throws IOException {
        this.stashedFileContent = EMPTY;
        this.stashedFileContent = readFileContent();
    }

    public void revertFile(String oldContent) {
        FilesUtils.toFile(filePath, oldContent);
    }

    public abstract void marshalParking(Parking parking) throws IOException;

    public abstract Parking unmarshalParking() throws IOException;

    public abstract void validateFile() throws IOException;

    public abstract String getMarshalledParking(Parking parking) throws IOException, JAXBException;
}