package com.mykolaiv.basecamp.summer2019.complextask01.service.mode.strategy;

import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.parking.Parking;
import com.mykolaiv.basecamp.summer2019.complextask01.service.exceptions.MarshallingException;
import com.mykolaiv.basecamp.summer2019.complextask01.service.exceptions.SchemaValidationException;
import com.mykolaiv.basecamp.summer2019.complextask01.utils.FilesUtils;
import com.mykolaiv.basecamp.summer2019.complextask01.service.marshallers.MarshallingHelper;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBException;
import java.io.IOException;

import static com.mykolaiv.basecamp.summer2019.complextask01.config.Configuration.XML_FILE_PATH;

/**
 * Application working mode that provides work with XML
 */
public class XmlModeStrategy extends ModeStrategy {

    public XmlModeStrategy() {
        super(XML_FILE_PATH);
    }

    @Override
    public Parking unmarshalParking() throws IOException {
        try {
            return MarshallingHelper.fromXml(readFileContent());
        } catch (JAXBException | SAXException e) {
            throw new MarshallingException(e);
        }
    }

    @Override
    public void validateFile() throws IOException {
        try {
            MarshallingHelper.validateXml(readFileContent());
        } catch (JAXBException | SAXException e) {
            throw new SchemaValidationException(e);
        }
    }

    @Override
    public String getMarshalledParking(Parking parking) {
        try {
            return MarshallingHelper.toXml(parking);
        } catch (JAXBException e) {
            throw new MarshallingException(e);
        }
    }

    @Override
    public void marshalParking(Parking parking) throws IOException {
        String marshalledParkingXml = getMarshalledParking(parking);
        FilesUtils.toFile(getFilePath(), marshalledParkingXml, false);
    }
}

