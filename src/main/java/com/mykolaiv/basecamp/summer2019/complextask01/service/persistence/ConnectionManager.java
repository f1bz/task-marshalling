package com.mykolaiv.basecamp.summer2019.complextask01.service.persistence;

import com.mykolaiv.basecamp.summer2019.complextask01.model.credentials.DatabaseCredentials;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Connection manager for getting JDBC connection
 */
public class ConnectionManager {

    private DatabaseCredentials databaseCredentials;

    public ConnectionManager(DatabaseCredentials databaseCredentials) {
        this.databaseCredentials = databaseCredentials;
    }

    /**
     * Test connection to database
     *
     * @throws ClassNotFoundException if driver not found
     * @throws SQLException           if can not get connection
     */
    void testConnection() throws ClassNotFoundException, SQLException {
        Class.forName(databaseCredentials.getDriver());
        getConnection().close();
    }

    /**
     * Create new connection to database
     *
     * @return Connection connection to database
     * @throws SQLException           if can not get connection
     */
    Connection getConnection() throws SQLException {
        return DriverManager.getConnection(
                databaseCredentials.getHostUrl(),
                databaseCredentials.getUser(),
                databaseCredentials.getPassword()
        );
    }
}