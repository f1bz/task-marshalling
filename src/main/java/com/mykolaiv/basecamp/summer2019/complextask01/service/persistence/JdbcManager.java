package com.mykolaiv.basecamp.summer2019.complextask01.service.persistence;

import com.mykolaiv.basecamp.summer2019.complextask01.repository.queries.Query;
import com.mykolaiv.basecamp.summer2019.complextask01.service.exceptions.UncheckedSQLException;
import com.mykolaiv.basecamp.summer2019.complextask01.service.exceptions.WrongQueryParameterException;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * JDBC Manager for execution scripts and queries
 */
public class JdbcManager {

    private final ConnectionManager connectionManager;

    public JdbcManager(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    /**
     * Executes query
     *
     * @param query  query object to run
     * @param params query param objects to be used in query
     * @return <T> result of query execution
     * @throws UncheckedSQLException        if there was SQL error
     * @throws WrongQueryParameterException if passed query params are not as supposed to be
     **/
    public <T> T executeQuery(Query query, Object... params) {
        try (Connection connection = connectionManager.getConnection()) {
            return query.getQueryExecutable().execute(connection, params);
        } catch (SQLException e) {
            throw new UncheckedSQLException(e);
        } catch (ClassCastException e) {
            throw new WrongQueryParameterException("Please check the params transferred to query", e);
        }
    }

    /**
     * Executes script
     *
     * @param script script to run
     * @throws UncheckedSQLException if there was SQL error
     **/
    public void executeScript(String script) {
        try (Connection connection = connectionManager.getConnection()) {
            ScriptRunner scriptRunner = new ScriptRunner();
            scriptRunner.executeScript(connection, script);
        } catch (SQLException e) {
            throw new UncheckedSQLException(e);
        }
    }

    /**
     * Test connection to database
     *
     * @throws UncheckedSQLException if connection not established
     **/
    public void testConnection() throws ClassNotFoundException {
        try {
            connectionManager.testConnection();
        } catch (SQLException e) {
            throw new UncheckedSQLException(e);
        }
    }
}