package com.mykolaiv.basecamp.summer2019.complextask01.service.persistence;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * ScriptRunner for execution multiple scripts by one query
 */
class ScriptRunner {

    private static final Pattern STATEMENTS_DELIMITERS = Pattern.compile("(?<=;)(?!\\s*END)|(?<=END;)");

    /**
     * Executes script
     *
     * @param connection connection to database
     * @param script     script to be executed
     * @throws SQLException if some mistake is present in the script
     */
    void executeScript(Connection connection, String script) throws SQLException {
        connection.setAutoCommit(false);
        try (Statement statement = connection.createStatement()) {
            for (String query : getSplittedQueries(script)) {
                if (!query.isEmpty()) {
                    statement.addBatch(query);
                }
            }
            statement.executeBatch();
        }
        connection.commit();
    }

    /**
     * Splits script into different queries
     *
     * @param script script to be splitted
     * @return List<String> list of splited queries
     */
    List<String> getSplittedQueries(String script) {
        List<String> stringsSplitted = new ArrayList<>();
        for (String s : STATEMENTS_DELIMITERS.split(script)) {
            if (!s.trim().isEmpty()) {
                stringsSplitted.add(s.trim());
            }
        }
        return stringsSplitted;
    }
}