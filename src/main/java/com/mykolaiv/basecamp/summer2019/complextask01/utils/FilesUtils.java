package com.mykolaiv.basecamp.summer2019.complextask01.utils;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.charset.Charset;

/**
 * Files util for reading/writing files
 */
public final class FilesUtils {

    private FilesUtils() {
        throw new IllegalStateException("Cannot create an instance of utility class");
    }

    public static String fromFile(String filename) throws IOException {
        return FileUtils.readFileToString(new File(filename), Charset.defaultCharset());
    }

    public static void toFile(String filename, String content) {
        try {
            toFile(filename, content, false);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public static void toFile(String filename, String content, boolean append) throws IOException {
        FileUtils.write(new File(filename), content, Charset.defaultCharset(), append);
    }
}