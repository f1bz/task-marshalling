drop table if exists parking;
drop table if exists cars;
drop table if exists car_parts;
drop table if exists operation_logs;

drop trigger IF exists logs_insertions_parking_trigger;
drop trigger IF exists logs_insertions_car_parts_trigger;
drop trigger IF exists logs_insertions_cars_trigger;

create table parking
(
    title               text not null,
    address                 text not null
);


create table cars
(
    number               text not null
        constraint cars_pk
            primary key,
    type                 text not null,
    brand                text not null,
    model                text not null,
    price_usd            int  not null,
    color                text not null,
    characteristic       text,
    car_owner_first_name text not null,
    car_owner_last_name  text not null
);

create unique index cars_number_uindex
    on cars (number);

create table car_parts
(
    name text not null,
    type text,
    other_characteristic text
);

create index car_parts_name_type_index
    on car_parts (name, type);

create table operation_logs
(
    id       integer primary key AUTOINCREMENT not null,
    datetime TIMESTAMP                         not null default CURRENT_TIMESTAMP,
    message  varchar(500)
);


CREATE TRIGGER logs_insertions_parking_trigger
    AFTER INSERT
    ON parking
BEGIN
    INSERT INTO operation_logs (message)
    VALUES ('Parkig created: ' || (NEW.address || ', ' || NEW.title)); END;

CREATE TRIGGER logs_insertions_car_parts_trigger
    AFTER INSERT
    ON car_parts
BEGIN
    INSERT INTO operation_logs (message)
    VALUES ('Car part inserted: ' || (NEW.name || ', ' || IFNULL(NEW.type,'') || ', ' || IFNULL(NEW.other_characteristic,''))); END;


CREATE TRIGGER logs_insertions_cars_trigger
    AFTER INSERT
    ON cars
BEGIN
    INSERT INTO operation_logs (message)
    VALUES ('Car inserted: ' || (NEW.number || ', ' || NEW.brand || ', ' || NEW.model || ', ' || NEW.color || ', ' ||
                                 NEW.price_usd || '$, ' || NEW.car_owner_first_name || ', ' || IFNULL(NEW.characteristic,'') || ','||
                                 NEW.car_owner_last_name)); END;
