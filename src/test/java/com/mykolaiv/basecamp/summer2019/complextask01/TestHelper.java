package com.mykolaiv.basecamp.summer2019.complextask01;

import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.CarOwner;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.Car;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.DefaultCar;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.SportCar;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.parts.CarConfigurationPart;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.parts.Engine;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.parts.Transmission;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.parts.TransmissionType;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.parking.Parking;

public class TestHelper {

    public static Parking createExampleParking() {
        Parking parking = new Parking("title", "address");
        parking.getParkedCars().add(createSportCar());
        parking.getParkedCars().add(createDefaultCar());
        return parking;
    }

    public static Parking createEmptyParking() {
        return new Parking("title", "address");
    }

    public static Car createSportCar() {
        Car sportCar = new SportCar(
                "regNum",
                "brand",
                "model",
                8000,
                "color",
                0
        );
        CarConfigurationPart engine = new Engine(5, "type");
        CarConfigurationPart transmission = new Transmission(TransmissionType.AUTO);

        sportCar.setCarOwner(new CarOwner("first name", "last name"));
        sportCar.addConfigurationPart(engine);
        sportCar.addConfigurationPart(transmission);
        return sportCar;
    }

    public static Car createDefaultCar() {
        Car defaultCar = new DefaultCar(
                "regNum_default",
                "brand_default",
                "model_default",
                1000,
                "color_default"
        );
        CarConfigurationPart engine = new Engine(1, "type_default");
        CarConfigurationPart transmission = new Transmission(TransmissionType.MANUAL);

        defaultCar.setCarOwner(new CarOwner("first name_default", "last name_default"));
        defaultCar.addConfigurationPart(engine);
        defaultCar.addConfigurationPart(transmission);
        return defaultCar;
    }
}