package com.mykolaiv.basecamp.summer2019.complextask01.config;

import com.mykolaiv.basecamp.summer2019.complextask01.ApplicationController;
import com.mykolaiv.basecamp.summer2019.complextask01.service.command.application.StartCommand;
import com.mykolaiv.basecamp.summer2019.complextask01.service.exceptions.PropertiesNotFoundException;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.Charset;

import static org.assertj.core.api.Assertions.assertThat;

@PrepareForTest(Configuration.class)
@RunWith(PowerMockRunner.class)
public class ConfigurationTest {

    private static final String APPLICATION_PROPERTIES_PATH = "/com/mykolaiv/basecamp/summer2019/complextask01/application.properties";

    @Before
    public void setUp() {
        PowerMockito.spy(Configuration.class);
    }

    @Test
    public void should_return_driver_property() throws Exception {
        PowerMockito.spy(Configuration.class);
        String properties = IOUtils.resourceToString(APPLICATION_PROPERTIES_PATH, Charset.defaultCharset());
        PowerMockito.doReturn(new BufferedReader(new StringReader(properties))).when(Configuration.class, "getReader");
        Configuration.initProperties();
        String actual = Configuration.getPropertyValue("database.driver");

        // Getting an UnfinishedVerificationException
//        verifyStatic(Configuration.class);
        assertThat(actual).isEqualTo("driver");
    }

    @Test
    public void value_should_be_empty_for_non_existent_property() throws Exception {
        PowerMockito.spy(Configuration.class);
        String properties = IOUtils.resourceToString(APPLICATION_PROPERTIES_PATH, Charset.defaultCharset());
        PowerMockito.doReturn(new BufferedReader(new StringReader(properties))).when(Configuration.class, "getReader");
        Configuration.initProperties();
        String actual = Configuration.getPropertyValue("database.menu");

        // Getting an UnfinishedVerificationException
//        verifyStatic(Configuration.class);
        assertThat(actual).isEmpty();
    }

    @Test(expected = PropertiesNotFoundException.class)
    public void properties_not_found_exception_expected() throws Exception {
        PowerMockito.spy(Configuration.class);
        PowerMockito.doThrow(new IOException()).when(Configuration.class, "initProperties");
        StartCommand startCommand = new StartCommand();
        startCommand.executeWith(new ApplicationController());

        // Getting an UnfinishedVerificationException
//        verifyStatic(Configuration.class);
    }
}