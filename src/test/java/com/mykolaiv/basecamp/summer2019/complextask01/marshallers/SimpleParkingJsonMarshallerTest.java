package com.mykolaiv.basecamp.summer2019.complextask01.marshallers;

import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.parking.Parking;
import com.mykolaiv.basecamp.summer2019.complextask01.service.marshallers.MarshallingHelper;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.Charset;

import static com.mykolaiv.basecamp.summer2019.complextask01.TestHelper.createExampleParking;
import static org.assertj.core.api.Assertions.assertThat;

public class SimpleParkingJsonMarshallerTest {

    private static final String JSON_PATH = "/com/mykolaiv/basecamp/summer2019/complextask01/json/parking.json";

    @Test
    public void to_json() throws IOException {
        String actual = MarshallingHelper.toJson(createExampleParking());
        String expected = IOUtils.resourceToString(JSON_PATH, Charset.defaultCharset());
        assertThat(actual).as("Resulting JSON should be same as in test file")
                .isEqualTo(expected);
    }

    @Test
    public void from_json() throws IOException {
        String json = IOUtils.resourceToString(JSON_PATH, Charset.defaultCharset());
        Parking actual = MarshallingHelper.fromJson(json);
        Parking expected = createExampleParking();
        assertThat(actual).as("Resulting unmarshalled parking should be same as an example")
                .isEqualToComparingFieldByField(expected);
    }
}