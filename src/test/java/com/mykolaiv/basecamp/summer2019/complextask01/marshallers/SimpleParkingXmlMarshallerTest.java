package com.mykolaiv.basecamp.summer2019.complextask01.marshallers;

import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.parking.Parking;
import com.mykolaiv.basecamp.summer2019.complextask01.service.marshallers.MarshallingHelper;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.nio.charset.Charset;

import static com.mykolaiv.basecamp.summer2019.complextask01.TestHelper.createExampleParking;
import static org.assertj.core.api.Assertions.assertThat;

public class SimpleParkingXmlMarshallerTest {

    private static final String XML_PATH = "/com/mykolaiv/basecamp/summer2019/complextask01/xml/parking.xml";

    @Test
    public void to_xml() throws IOException, JAXBException {
        String actual = MarshallingHelper.toXml(createExampleParking());
        String expected = IOUtils.resourceToString(XML_PATH, Charset.defaultCharset());
        assertThat(actual).as("Resulting XML should be same as in test file")
                .isEqualTo(expected);
    }

    @Test
    public void from_xml() throws IOException, JAXBException, SAXException {
        String xml = IOUtils.resourceToString(XML_PATH, Charset.defaultCharset());
        Parking actual = MarshallingHelper.fromXml(xml);
        Parking expected = createExampleParking();
        assertThat(actual).as("Resulting unmarshalled parking should be same as an example")
                .isEqualToComparingFieldByField(expected);
    }

}