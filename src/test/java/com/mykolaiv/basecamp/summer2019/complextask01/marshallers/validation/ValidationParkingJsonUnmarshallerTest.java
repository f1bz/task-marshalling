package com.mykolaiv.basecamp.summer2019.complextask01.marshallers.validation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.parking.Parking;
import com.mykolaiv.basecamp.summer2019.complextask01.service.marshallers.MarshallingHelper;
import org.junit.Test;

import java.io.IOException;

import static com.mykolaiv.basecamp.summer2019.complextask01.TestHelper.createExampleParking;

public class ValidationParkingJsonUnmarshallerTest {

    @Test
    public void validate_normal_json() throws IOException {
        Parking parking = createExampleParking();
        MarshallingHelper.validateJson(MarshallingHelper.toJson(parking));
    }

    @Test(expected = JsonProcessingException.class)
    public void should_fail_on_validation_broken_json() throws IOException {
        Parking parking = createExampleParking();
        String badJson = MarshallingHelper.toJson(parking)
                .replace("address", "wrong key");
        MarshallingHelper.validateJson(badJson);
    }
}