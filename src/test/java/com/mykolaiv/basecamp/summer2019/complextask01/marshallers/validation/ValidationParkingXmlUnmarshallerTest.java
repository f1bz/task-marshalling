package com.mykolaiv.basecamp.summer2019.complextask01.marshallers.validation;

import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.parking.Parking;
import com.mykolaiv.basecamp.summer2019.complextask01.service.marshallers.MarshallingHelper;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBException;

import static com.mykolaiv.basecamp.summer2019.complextask01.TestHelper.createExampleParking;

public class ValidationParkingXmlUnmarshallerTest {

    @Test
    public void validate_normal_xml() throws JAXBException, SAXException {
        Parking parking = createExampleParking();
        MarshallingHelper.validateXml(MarshallingHelper.toXml(parking));
    }

    @Test(expected = JAXBException.class)
    public void should_fail_on_validation_broken_xml() throws JAXBException, SAXException {
        Parking parking = createExampleParking();
        String badJson = MarshallingHelper.toXml(parking)
                .replace("address", "wrong key");
        MarshallingHelper.validateXml(badJson);
    }
}