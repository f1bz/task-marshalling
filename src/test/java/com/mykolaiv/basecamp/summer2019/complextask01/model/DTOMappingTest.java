package com.mykolaiv.basecamp.summer2019.complextask01.model;

import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.parking.Parking;
import com.mykolaiv.basecamp.summer2019.complextask01.model.dto.CarConfigurationPartDTO;
import com.mykolaiv.basecamp.summer2019.complextask01.model.dto.CarDTO;
import com.mykolaiv.basecamp.summer2019.complextask01.model.dto.ParkingDTO;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static com.mykolaiv.basecamp.summer2019.complextask01.TestHelper.createExampleParking;
import static org.assertj.core.api.Java6Assertions.assertThat;

public class DTOMappingTest {

    @Test
    public void map_parking_to_DTO() {
        CarConfigurationPartDTO carPartDto1 = new CarConfigurationPartDTO();
        carPartDto1.setType("type");
        carPartDto1.setName("Engine");
        carPartDto1.setCharacteristic("5.0");

        CarConfigurationPartDTO carPartDto2 = new CarConfigurationPartDTO();
        carPartDto2.setType("AUTO");
        carPartDto2.setName("Transmission");

        CarConfigurationPartDTO carPartDto3 = new CarConfigurationPartDTO();
        carPartDto3.setType("type_default");
        carPartDto3.setName("Engine");
        carPartDto3.setCharacteristic("1.0");

        CarConfigurationPartDTO carPartDto4 = new CarConfigurationPartDTO();
        carPartDto4.setType("MANUAL");
        carPartDto4.setName("Transmission");

        CarDTO sportCarDTO = new CarDTO();
        sportCarDTO.setBrand("brand");
        sportCarDTO.setModel("model");
        sportCarDTO.setColor("color");
        sportCarDTO.setPrice(8000);
        sportCarDTO.setOwnerFirstName("first name");
        sportCarDTO.setOwnerLastName("last name");
        sportCarDTO.setCharacteristic("0");
        sportCarDTO.setRegistrationNumber("regNum");
        sportCarDTO.setType("SportCar");

        CarDTO defaultCarDTO = new CarDTO();
        defaultCarDTO.setBrand("brand_default");
        defaultCarDTO.setModel("model_default");
        defaultCarDTO.setColor("color_default");
        defaultCarDTO.setPrice(1000);
        defaultCarDTO.setOwnerFirstName("first name_default");
        defaultCarDTO.setOwnerLastName("last name_default");
        defaultCarDTO.setRegistrationNumber("regNum_default");
        defaultCarDTO.setType("DefaultCar");

        List<CarConfigurationPartDTO> partDTOs = Arrays.asList(carPartDto1, carPartDto2, carPartDto3, carPartDto4);
        List<CarDTO> carDTOs = Arrays.asList(sportCarDTO, defaultCarDTO);

        String title = "title";
        String address = "address";

        ParkingDTO expected = new ParkingDTO(title,address, carDTOs, partDTOs);

        Parking parking = createExampleParking();
        ParkingDTO actual = ParkingDTO.mapToDTO(parking);

        assertThat(actual).as("Resulting mapped parkingDTO be same as a mapped one from example parking").isEqualTo(expected);
    }

    @Test(expected = NullPointerException.class)
    public void map_null_parking_to_DTO() {
        Parking parking = null;
        ParkingDTO.mapToDTO(parking);
    }
}