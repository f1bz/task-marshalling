package com.mykolaiv.basecamp.summer2019.complextask01.model;

import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.Car;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.parking.Parking;
import org.junit.Test;

import static com.mykolaiv.basecamp.summer2019.complextask01.TestHelper.*;
import static org.assertj.core.api.Java6Assertions.assertThat;

public class ParkingOperationsTest {

    @Test
    public void park_cars() {
        Parking actual = createEmptyParking();

        Car sportCar = createSportCar();
        actual.parkCar(sportCar);
        Car defaultCar = createDefaultCar();
        actual.parkCar(defaultCar);

        assertThat(actual.getParkedCars()).hasSize(2)
                .contains(sportCar, defaultCar);
    }

    @Test
    public void un_park_all_car() {
        Parking actual = createExampleParking();

        actual.unParkCar(0);
        actual.unParkCar(0);

        assertThat(actual.getParkedCars()).hasSize(0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void un_park_car_in_empty_parking() {
        Parking actual = createEmptyParking();
        actual.unParkCar(0);
    }
}
