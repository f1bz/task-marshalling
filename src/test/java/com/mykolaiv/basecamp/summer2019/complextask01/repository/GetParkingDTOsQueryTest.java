package com.mykolaiv.basecamp.summer2019.complextask01.repository;

import com.mykolaiv.basecamp.summer2019.complextask01.model.dto.ParkingDTO;
import com.mykolaiv.basecamp.summer2019.complextask01.repository.queries.parking.GetParkingDTOsQuery;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GetParkingDTOsQueryTest {

    @Mock
    Connection connection;

    @Test
    public void should_return_2_values_after_selecting_parkings_query() throws SQLException {
        GetParkingDTOsQuery query = new GetParkingDTOsQuery();

        PreparedStatement statement = mock(PreparedStatement.class);
        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet.next())
                .thenReturn(true)
                .thenReturn(true)
                .thenReturn(false);

        when(resultSet.getString(1))
                .thenReturn("title 1")
                .thenReturn("title 2");
        when(resultSet.getString(2))
                .thenReturn("address 1")
                .thenReturn("address 2");

        when(statement.executeQuery()).thenReturn(resultSet);
        when(connection.prepareStatement(any(String.class))).thenReturn(statement);

        List<ParkingDTO> result = query.execute(connection);

        assertThat(result).hasSize(2)
                .contains(new ParkingDTO("title 1", "address 1", null, null))
                .contains(new ParkingDTO("title 2", "address 2", null, null));
    }
}
