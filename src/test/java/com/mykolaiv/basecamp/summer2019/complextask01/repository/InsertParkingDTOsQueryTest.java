package com.mykolaiv.basecamp.summer2019.complextask01.repository;

import com.mykolaiv.basecamp.summer2019.complextask01.repository.queries.Query;
import com.mykolaiv.basecamp.summer2019.complextask01.service.exceptions.WrongQueryParameterException;
import com.mykolaiv.basecamp.summer2019.complextask01.service.persistence.ConnectionManager;
import com.mykolaiv.basecamp.summer2019.complextask01.service.persistence.JdbcManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

import java.sql.Connection;

import static org.powermock.api.mockito.PowerMockito.doReturn;
import static org.powermock.api.mockito.PowerMockito.mock;

@RunWith(PowerMockRunner.class)
public class InsertParkingDTOsQueryTest {

    @Test(expected = WrongQueryParameterException.class)
    public void should_fail_while_inserting_parkings_query_with_wrong_param() throws Exception {
        Connection connection = mock(Connection.class);
        ConnectionManager connectionManager = mock(ConnectionManager.class);
        doReturn(connection).when(connectionManager, "getConnection");
        JdbcManager jdbcManager = new JdbcManager(connectionManager);
        Object notParkingObject = new Object();
        jdbcManager.executeQuery(Query.INSERT_PARKING_QUERY, notParkingObject);
    }
}
