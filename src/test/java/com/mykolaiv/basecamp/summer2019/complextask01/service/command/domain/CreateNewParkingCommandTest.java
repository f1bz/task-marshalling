package com.mykolaiv.basecamp.summer2019.complextask01.service.command.domain;

import com.mykolaiv.basecamp.summer2019.complextask01.ApplicationController;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.parking.Parking;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CreateNewParkingCommandTest {

    @Test
    public void should_create_new_parking_command() {
        CreateNewParkingCommand createNewParkingCommand = new CreateNewParkingCommand();
        ApplicationController applicationController = new ApplicationController();
        createNewParkingCommand.executeWith(applicationController);
        Parking expected = new Parking("title", "address");
        Parking actual = applicationController.getParking();

        assertThat(actual).isEqualToComparingFieldByField(expected);
    }
}