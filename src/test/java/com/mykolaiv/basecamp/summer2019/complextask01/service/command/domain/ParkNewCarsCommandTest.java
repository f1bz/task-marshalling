package com.mykolaiv.basecamp.summer2019.complextask01.service.command.domain;

import com.mykolaiv.basecamp.summer2019.complextask01.ApplicationController;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.Car;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.parking.Parking;
import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ParkNewCarsCommandTest {
    @Test
    public void should_have_5_parked_new_cars() {
        ApplicationController applicationController = new ApplicationController();
        applicationController.setParking(new Parking(null, null));
        ParkNewCarsCommand parkNewCarsCommand = new ParkNewCarsCommand();
        parkNewCarsCommand.executeWith(applicationController);
        List<Car> parkedCars = applicationController.getParking().getParkedCars();

        assertThat(parkedCars).hasSize(5)
                .as("There should be 5 cars with following brand")
                .extracting("brand")
                .contains("Nissan", "Opel", "Nissan", "Opel", "Nissan");
    }
}