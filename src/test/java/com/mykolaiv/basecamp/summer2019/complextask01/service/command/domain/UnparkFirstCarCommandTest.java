package com.mykolaiv.basecamp.summer2019.complextask01.service.command.domain;

import com.mykolaiv.basecamp.summer2019.complextask01.ApplicationController;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.car.Car;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.parking.Parking;
import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class UnparkFirstCarCommandTest{

    @Test
    public void un_park_3_cars_should_left_2_cars() {
        ApplicationController applicationController = new ApplicationController();
        applicationController.setParking(new Parking(null, null));
        ParkNewCarsCommand parkNewCarsCommand = new ParkNewCarsCommand();
        parkNewCarsCommand.executeWith(applicationController);

        UnparkFirstCarCommand unparkFirstCarCommand = new UnparkFirstCarCommand();
        unparkFirstCarCommand.executeWith(applicationController);
        unparkFirstCarCommand.executeWith(applicationController);
        unparkFirstCarCommand.executeWith(applicationController);
        List<Car> parkedCars = applicationController.getParking().getParkedCars();

        assertThat(parkedCars).hasSize(2)
                .as("There should be 2 cars with following brand")
                .extracting("brand")
                .contains("Nissan", "Opel");
    }
}