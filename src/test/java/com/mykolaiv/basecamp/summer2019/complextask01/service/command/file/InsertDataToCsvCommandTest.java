package com.mykolaiv.basecamp.summer2019.complextask01.service.command.file;

import com.mykolaiv.basecamp.summer2019.complextask01.ApplicationController;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.parking.Parking;
import com.mykolaiv.basecamp.summer2019.complextask01.model.dto.CarConfigurationPartDTO;
import com.mykolaiv.basecamp.summer2019.complextask01.model.dto.CarDTO;
import com.mykolaiv.basecamp.summer2019.complextask01.model.dto.ParkingDTO;
import com.mykolaiv.basecamp.summer2019.complextask01.repository.JdbcParkingRepository;
import com.mykolaiv.basecamp.summer2019.complextask01.service.persistence.JdbcManager;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mockito.Mockito;
import org.powermock.reflect.Whitebox;

import java.nio.charset.Charset;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static com.mykolaiv.basecamp.summer2019.complextask01.TestHelper.createExampleParking;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class InsertDataToCsvCommandTest {

    private static final String EXPECTED_CSV_PATH = "/com/mykolaiv/basecamp/summer2019/complextask01/csv/expected.csv";

    @Test
    public void data_to_csv() throws Exception {
        ApplicationController controller = Mockito.spy(ApplicationController.class);

        Parking parking = createExampleParking();
        List<CarConfigurationPartDTO> partDTOs = parking.getParkedCars().stream()
                .flatMap(car -> car.getCarConfigurationPartList().stream())
                .map(CarConfigurationPartDTO::mapToDTO)
                .collect(Collectors.toList());
        List<CarDTO> carDTOs = parking.getParkedCars().stream()
                .map(CarDTO::mapToDTO)
                .collect(Collectors.toList());
        List<ParkingDTO> parkingDTOs = Collections.singletonList(ParkingDTO.mapToDTO(parking));

        JdbcManager jdbcManager = mock(JdbcManager.class);
        controller.setParkingRepository(new JdbcParkingRepository(jdbcManager));

        when(controller.getParkingRepository().getCarConfigurationPartDTOs()).thenReturn(partDTOs);
        when(controller.getParkingRepository().getCarDTOs()).thenReturn(carDTOs);
        when(controller.getParkingRepository().getParkingDTOs()).thenReturn(parkingDTOs);

        InsertDataToCsvCommand command = new InsertDataToCsvCommand();
        String actual = Whitebox.invokeMethod(command, "generateCsvData", controller).toString();
        String expected = IOUtils.resourceToString(EXPECTED_CSV_PATH, Charset.defaultCharset());

        verify(controller, times(4)).getParkingRepository();
        assertThat(actual).isEqualTo(expected);
    }
}