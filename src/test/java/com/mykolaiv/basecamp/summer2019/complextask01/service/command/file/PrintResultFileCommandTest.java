package com.mykolaiv.basecamp.summer2019.complextask01.service.command.file;

import com.mykolaiv.basecamp.summer2019.complextask01.service.mode.Mode;
import com.mykolaiv.basecamp.summer2019.complextask01.service.mode.strategy.ModeStrategy;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.only;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.spy;
import static org.powermock.api.mockito.PowerMockito.when;

@PrepareForTest(Mode.class)
@RunWith(PowerMockRunner.class)
public class PrintResultFileCommandTest {

    @Test
    public void read_file_content() throws Exception {
        ModeStrategy modeStrategy = mock(ModeStrategy.class);
        when(modeStrategy.readFileContent()).thenReturn("test file content");
        PrintResultFileCommand printResultFileCommand = spy(new PrintResultFileCommand());
        String actual = Whitebox.invokeMethod(printResultFileCommand, "getFileContent", modeStrategy);
        String expected = "test file content";

        Mockito.verify(modeStrategy, only()).readFileContent();
        assertThat(actual).isEqualTo(expected);
    }
}