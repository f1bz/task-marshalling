package com.mykolaiv.basecamp.summer2019.complextask01.service.command.file;

import com.mykolaiv.basecamp.summer2019.complextask01.ApplicationController;
import com.mykolaiv.basecamp.summer2019.complextask01.service.mode.Mode;
import com.mykolaiv.basecamp.summer2019.complextask01.service.mode.strategy.ModeStrategy;
import com.mykolaiv.basecamp.summer2019.complextask01.utils.FilesUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.spy;
import static org.powermock.api.mockito.PowerMockito.when;

@PrepareForTest({Mode.class, FilesUtils.class})
@RunWith(PowerMockRunner.class)
public class RevertStashFileCommandsTest {

    @Test
    public void revert_file_changes() throws Exception {
        PowerMockito.mockStatic(FilesUtils.class);
        PowerMockito.doNothing().when(FilesUtils.class, "toFile", anyString(), anyString());

        Mode mode = mock(Mode.class);
        ModeStrategy modeStrategy = mock(ModeStrategy.class);
        when(mode.getModeStrategy()).thenReturn(modeStrategy);

        when(modeStrategy.readFileContent()).thenReturn("oldContent");
        when(modeStrategy.getStashedFileContent()).thenReturn("newContent");

        ApplicationController applicationController = spy(new ApplicationController());
        when(applicationController.getCurrentMode()).thenReturn(mode);

        StashFileContentCommand stashFileContentCommand = new StashFileContentCommand();
        stashFileContentCommand.executeWith(applicationController);

        RevertFileChangesCommand revertFileChangesCommand = new RevertFileChangesCommand();
        revertFileChangesCommand.executeWith(applicationController);

        String actual = modeStrategy.readFileContent();
        String expected = "oldContent";

        Mockito.verify(mode, times(2)).getModeStrategy();
        Mockito.verify(modeStrategy, times(1)).readFileContent();
        Mockito.verify(modeStrategy, times(1)).getStashedFileContent();
        assertThat(actual).isEqualTo(expected);

    }
}