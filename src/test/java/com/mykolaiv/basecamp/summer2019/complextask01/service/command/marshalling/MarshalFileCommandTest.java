package com.mykolaiv.basecamp.summer2019.complextask01.service.command.marshalling;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.mykolaiv.basecamp.summer2019.complextask01.ApplicationController;
import com.mykolaiv.basecamp.summer2019.complextask01.service.exceptions.MarshallingException;
import com.mykolaiv.basecamp.summer2019.complextask01.service.mode.Mode;
import com.mykolaiv.basecamp.summer2019.complextask01.service.mode.strategy.JsonModeStrategy;
import com.mykolaiv.basecamp.summer2019.complextask01.service.mode.strategy.ModeStrategy;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Mode.class})
public class MarshalFileCommandTest {

    @Test(expected = MarshallingException.class)
    public void json_exception_while_marhsalling() throws IOException {
        ModeStrategy modeStrategy = mock(JsonModeStrategy.class);
        Mode mode = mock(Mode.class);
        PowerMockito.doReturn(modeStrategy).when(mode).getModeStrategy();
        PowerMockito.doThrow(new JsonMappingException("")).when(modeStrategy).marshalParking(any());
        ApplicationController controller = new ApplicationController();
        controller.setCurrentMode(mode);
        MarshalFileCommand marshalFileCommand = new MarshalFileCommand();
        marshalFileCommand.executeWith(controller);
    }
}