package com.mykolaiv.basecamp.summer2019.complextask01.service.command.marshalling;

import com.mykolaiv.basecamp.summer2019.complextask01.ApplicationController;
import com.mykolaiv.basecamp.summer2019.complextask01.service.exceptions.SchemaValidationException;
import com.mykolaiv.basecamp.summer2019.complextask01.service.mode.Mode;
import com.mykolaiv.basecamp.summer2019.complextask01.service.mode.strategy.JsonModeStrategy;
import com.mykolaiv.basecamp.summer2019.complextask01.service.mode.strategy.ModeStrategy;
import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;

import static org.mockito.Mockito.mock;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Mode.class})
public class ValidateFileCommandTest {

    @Test(expected = SchemaValidationException.class)
    public void schema_validate_exception() throws IOException {
        ModeStrategy modeStrategy = mock(JsonModeStrategy.class);
        Mode mode = mock(Mode.class);
        PowerMockito.doReturn(modeStrategy).when(mode).getModeStrategy();
        PowerMockito.doThrow(new JSONException("")).when(modeStrategy).validateFile();
        ApplicationController controller = new ApplicationController();
        controller.setCurrentMode(mode);
        ValidateFileCommand validateFileCommand = new ValidateFileCommand();
        validateFileCommand.executeWith(controller);
    }
}