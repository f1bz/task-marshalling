package com.mykolaiv.basecamp.summer2019.complextask01.service.command.persistence;

import com.mykolaiv.basecamp.summer2019.complextask01.ApplicationController;
import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.parking.Parking;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
public class PersistParkingCommandTest {

    @Test(expected = IllegalStateException.class)
    public void save_parking_with_no_title() {
        ApplicationController controller = new ApplicationController();
        controller.setParking(new Parking(null, null));
        PersistParkingCommand persistParkingCommand = new PersistParkingCommand();
        persistParkingCommand.executeWith(controller);
    }

    @Test(expected = NullPointerException.class)
    public void save_null_parking() {
        ApplicationController controller = new ApplicationController();
        controller.setParking(null);
        PersistParkingCommand persistParkingCommand = new PersistParkingCommand();
        persistParkingCommand.executeWith(controller);
    }
}