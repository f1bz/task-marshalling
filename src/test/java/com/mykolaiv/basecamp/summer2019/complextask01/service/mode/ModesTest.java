package com.mykolaiv.basecamp.summer2019.complextask01.service.mode;

import com.mykolaiv.basecamp.summer2019.complextask01.model.domain.parking.Parking;
import com.mykolaiv.basecamp.summer2019.complextask01.service.exceptions.SchemaValidationException;
import com.mykolaiv.basecamp.summer2019.complextask01.service.mode.strategy.JsonModeStrategy;
import com.mykolaiv.basecamp.summer2019.complextask01.service.mode.strategy.ModeStrategy;
import com.mykolaiv.basecamp.summer2019.complextask01.service.mode.strategy.XmlModeStrategy;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collection;

import static com.mykolaiv.basecamp.summer2019.complextask01.TestHelper.createExampleParking;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

@RunWith(Parameterized.class)
public class ModesTest {

    private static final String JSON_PATH = "/com/mykolaiv/basecamp/summer2019/complextask01/json/parking.json";
    private static final String XML_PATH = "/com/mykolaiv/basecamp/summer2019/complextask01/xml/parking.xml";

    @Parameterized.Parameters(name = "{0}")
    public static Collection<Object[]> data() throws IOException {
        return Arrays.asList(new Object[][]{
                {"JsonMode", spy(JsonModeStrategy.class), IOUtils.resourceToString(JSON_PATH, Charset.defaultCharset()), createExampleParking()},
                {"XmlMode", spy(XmlModeStrategy.class), IOUtils.resourceToString(XML_PATH, Charset.defaultCharset()), createExampleParking()},
        });
    }

    private ModeStrategy modeStrategy;
    private String marshallingResult;
    private Parking unmarshallingResult;

    public ModesTest(String testName, ModeStrategy modeStrategy, String marshallingResult, Parking unmarshallingResult) {
        this.modeStrategy = modeStrategy;
        this.unmarshallingResult = unmarshallingResult;
        this.marshallingResult = marshallingResult;
    }

    @Test
    public void marshall_parking() throws IOException, JAXBException {
        String marshaledParking = modeStrategy.getMarshalledParking(unmarshallingResult);
        assertThat(marshaledParking).isEqualTo(marshallingResult);
    }

    @Test
    public void unmarshal_parking() throws IOException {
        when(modeStrategy.readFileContent()).thenReturn(marshallingResult);
        Parking actual = modeStrategy.unmarshalParking();
        assertThat(actual).isEqualToComparingFieldByField(unmarshallingResult);
    }

    @Test
    public void validate_normal() throws IOException {
        when(modeStrategy.readFileContent()).thenReturn(marshallingResult);
        modeStrategy.validateFile();
    }

    @Test(expected = SchemaValidationException.class)
    public void should_fail_on_validation_broken_source() throws IOException {
        when(modeStrategy.readFileContent()).thenReturn(marshallingResult.replace("title", "broken"));
        modeStrategy.validateFile();
    }
}