package com.mykolaiv.basecamp.summer2019.complextask01.service.persistence;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ScriptRunnerTest {

    private static final String CREATE_SCHEMA_SCRIPT = "/com/mykolaiv/basecamp/summer2019/complextask01/database/create-schema.sql";

    @Test
    public void should_split_to_16_queries() throws IOException {
        ScriptRunner scriptRunner = new ScriptRunner();
        String script = IOUtils.resourceToString(CREATE_SCHEMA_SCRIPT, Charset.defaultCharset());
        List<String> splittedQueries = scriptRunner.getSplittedQueries(script);
        assertThat(splittedQueries).hasSize(16);
    }
}
