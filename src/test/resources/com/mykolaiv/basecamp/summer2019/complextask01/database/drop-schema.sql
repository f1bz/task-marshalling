drop table if exists parking;
drop table if exists cars;
drop table if exists car_parts;
drop table if exists operation_logs;

drop trigger IF exists logs_insertions_parking_trigger;
drop trigger IF exists logs_insertions_car_parts_trigger;
drop trigger IF exists logs_insertions_cars_trigger;